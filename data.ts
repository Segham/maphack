import { Ability, Color, Entity, GameSleeper, Hero, Item, LinearProjectile, Modifier, Particle, Sleeper, Team, Unit, Vector3, WardObserver, WardTrueSight } from "wrapper/Imports"
import { ParticlesX } from "X-Core/Imports"
import { AbilitiesMap, IceBlastMap, ItemsMap, PudgeHookMap, TeleportsMap, WardDispenserCount } from "./IType"
import { CreepWave, Teleports, WispModel } from "./Models/index"
import { TemplarAssasinModel } from "./Models/Templar"

export enum LanePosition {
	Unknown,
	Easy,
	Middle,
	Hard,
}

export interface ItemsDataX {
	path: string,
	item: typeof Item,
	timeToShow: number,
	ctrPoint: number,
	range?: number
}

export interface AbilityDataX {
	path: string,
	abil: typeof Ability,
	ctrPoint?: number,
	ctrPointEnd?: number,
	released?: boolean,
	range?: boolean,
	rangeEnd?: boolean,
	noSetZ?: boolean
	isVisible?: boolean,
	swapTarget?: boolean,
	findOwner?: boolean,
	timeToShow?: number,
	replace?: boolean,
	delete?: boolean,
	rectangle?: boolean,
	rangeTimer?: boolean,
	showUtilTime?: number,
	excludeIsVisible?: boolean
}

export type IEntitiesData = {
	minimap?: boolean
	nameIcon: string,
	AbilityName: string,
	AOERadius: (abil: Ability) => number,
	Duration?: (abil: Ability) => number,
}

export interface IDATAWards {
	hero: Unit
	type: string,
	time: number,
	position: Vector3,
	particleID: number,
	entity: Nullable<WardObserver | WardTrueSight>
}

export class MAP_HACK_LANE_PATCH {

	public direBotPath: Vector3[] = [
		new Vector3(6239, 3717, 384),
		new Vector3(6166, 3097, 384),
		new Vector3(6407, 635, 384),
		new Vector3(6422, -816, 384),
		new Vector3(6477, -2134, 384),
		new Vector3(5995, -3054, 384),
		new Vector3(6071, -3601, 384),
		new Vector3(6127, -4822, 384),
		new Vector3(5460, -5893, 384),
		new Vector3(5070, -5915, 384),
		new Vector3(4412, -6048, 384),
		new Vector3(3388, -6125, 384),
		new Vector3(2641, -6135, 384),
		new Vector3(1320, -6374, 384),
		new Vector3(-174, -6369, 384),
		new Vector3(-1201, -6308, 384),
		new Vector3(-3036, -6072, 257),
	]

	public direMidPath: Vector3[] = [
		new Vector3(4101, 3652, 384),
		new Vector3(3549, 3041, 256),
		new Vector3(2639, 2061, 256),
		new Vector3(2091, 1605, 256),
		new Vector3(1091, 845, 256),
		new Vector3(-163, -62, 256),
		new Vector3(-789, -579, 178),
		new Vector3(-1327, -1017, 256),
		new Vector3(-2211, -1799, 256),
		new Vector3(-2988, -2522, 256),
		new Vector3(-4103, -3532, 256),
	]

	public direTopPath: Vector3[] = [
		new Vector3(3154, 5811, 384),
		new Vector3(2903, 5818, 299),
		new Vector3(-248, 5852, 384),
		new Vector3(-1585, 5979, 384),
		new Vector3(-3253, 5981, 384),
		new Vector3(-3587, 5954, 384),
		new Vector3(-3954, 5860, 384),
		new Vector3(-4988, 5618, 384),
		new Vector3(-5714, 5514, 384),
		new Vector3(-6051, 5092, 384),
		new Vector3(-6299, 4171, 384),
		new Vector3(-6342, 3806, 384),
		new Vector3(-6419, 2888, 384),
		new Vector3(-6434, -2797, 256),
	]

	public radiantBotPath: Vector3[] = [
		new Vector3(-3628, -6121, 384),
		new Vector3(-3138, -6168, 256),
		new Vector3(-2126, -6234, 256),
		new Vector3(-1064, -6383, 384),
		new Vector3(196, -6602, 384),
		new Vector3(1771, -6374, 384),
		new Vector3(3740, -6281, 384),
		new Vector3(5328, -5813, 384),
		new Vector3(5766, -5602, 384),
		new Vector3(6265, -4992, 384),
		new Vector3(6112, -3603, 384),
		new Vector3(6103, 1724, 256),
		new Vector3(6133, 2167, 256),
	]

	public radiantMidPath: Vector3[] = [
		new Vector3(-5000, -4458, 384),
		new Vector3(-4775, -4020, 384),
		new Vector3(-4242, -3594, 256),
		new Vector3(-3294, -2941, 256),
		new Vector3(-2771, -2454, 256),
		new Vector3(-2219, -1873, 256),
		new Vector3(-1193, -1006, 256),
		new Vector3(-573, -449, 128),
		new Vector3(-46, -49, 256),
		new Vector3(679, 461, 256),
		new Vector3(979, 692, 256),
		new Vector3(2167, 1703, 256),
		new Vector3(2919, 2467, 256),
		new Vector3(3785, 3132, 256),
	]

	public radiantTopPath: Vector3[] = [
		new Vector3(-6604, -3979, 384),
		new Vector3(-6613, -3679, 384),
		new Vector3(-6775, -3420, 384),
		new Vector3(-6743, -3042, 369),
		new Vector3(-6682, -2124, 256),
		new Vector3(-6640, -1758, 384),
		new Vector3(-6411, -226, 384),
		new Vector3(-6370, 2523, 384),
		new Vector3(-6198, 3997, 384),
		new Vector3(-6015, 4932, 384),
		new Vector3(-5888, 5204, 384),
		new Vector3(-5389, 5548, 384),
		new Vector3(-4757, 5740, 384),
		new Vector3(-4066, 5873, 384),
		new Vector3(-3068, 6009, 384),
		new Vector3(-2139, 6080, 384),
		new Vector3(-1327, 6052, 384),
		new Vector3(-82, 6011, 384),
		new Vector3(1682, 5993, 311),
		new Vector3(2404, 6051, 256),
	]

	public Lanes: Map<LanePosition, Vector3[]> = new Map<LanePosition, Vector3[]>();

	public Init(team: Team) {
		if (this.Lanes.size >= 3)
			return

		if (team === Team.Radiant) {
			this.Lanes.set(LanePosition.Easy, this.direTopPath)
			this.Lanes.set(LanePosition.Middle, this.direMidPath)
			this.Lanes.set(LanePosition.Hard, this.direBotPath)
			return
		}
		this.Lanes.set(LanePosition.Easy, this.radiantBotPath)
		this.Lanes.set(LanePosition.Middle, this.radiantMidPath)
		this.Lanes.set(LanePosition.Hard, this.radiantTopPath)
	}

	public GetCreepLane(unit: Unit): LanePosition {
		let lanePath = LanePosition.Unknown
		this.Lanes.forEach((vec, lane) => {
			if (unit.Distance(vec[0]) < 500) {
				lanePath = lane
				return
			}
		})
		return lanePath
	}

	public GetLanePath(lane: LanePosition): Vector3[] {
		const getLine = this.Lanes.get(lane)
		if (getLine === undefined)
			return []

		return getLine
	}
}

export class MAP_HACK_DATA {

	public static CreepWaveIndex = 0
	public static WardCaptureTiming = 0
	public static IceBlastFinal = false
	public static TickWardPlace = false
	public static RunTimeCheckWard = false
	public static TeleportCheckRadius = 1150
	public static TeleportCheckDuration = 25000
	public static Units: Unit[] = []
	public static TELEPORTS: Teleports[] = []
	public static CreepWaves: CreepWave[] = []
	public static WardsData: IDATAWards[] = []

	public static GameSleeper = new GameSleeper()
	public static ParticleSDK = new ParticlesX()
	public static lanePaths = new MAP_HACK_LANE_PATCH()

	public static AbilitiesList: Ability[] = []
	public static LinearProjectile: LinearProjectile[] = []
	public static ModifierTime = new Map<Modifier, number>()

	public static RunTimeParticles = new Map<number, Particle>()
	public static TeleportSleepers = new Map<Vector3, Sleeper>()

	public static JungleAttackCreeps: [Hero, Vector3[], number][] = []
	public static WardsDataRunTime: (WardObserver | WardTrueSight)[] = []
	public static AbilitiesSpecialUnits = new Map<number, [IEntitiesData, Unit | Vector3, number, number?]>()

	public static ItemsMap: ItemsMap = new Map()
	public static IceBlastMap: IceBlastMap = new Map()
	public static TeleportsMap: TeleportsMap = new Map()
	public static PudgeHookMap: PudgeHookMap = new Map()
	public static AbilitiesMap: AbilitiesMap = new Map()
	public static WardDispenserCount: WardDispenserCount = new Map()
	public static SoundAbilities = new Map<number | string, [string, Vector3, Unit | Ability, number]>()

	public static WaitingExplode = new Map<number, string>()
	public static ThinkerScan = new Map<number, Modifier>()
	public static WaitingSpawn = new Map<number, [string, Entity?]>()

	public static AllTechiesMines: [[Vector3, number, number, string][], Vector3, string, Ability][] = []
	public static AllTechiesMinesMap = new Map<number, [number, Vector3, string, Ability, boolean]>()

	public static PlColor = new Map<Color, number>()
		.set(new Color(51, 117, 255), 0)
		.set(new Color(102, 255, 191), 1)
		.set(new Color(191, 0, 191), 2)
		.set(new Color(243, 240, 11), 3)
		.set(new Color(255, 107, 0), 4)
		.set(new Color(254, 134, 194), 5)
		.set(new Color(161, 180, 71), 6)
		.set(new Color(101, 217, 247), 7)
		.set(new Color(0, 131, 33), 8)
		.set(new Color(164, 105, 0), 9)

	public static Dispose() {
		this.Units = []
		this.TELEPORTS = []
		this.CreepWaves = []
		this.WardsData = []
		this.WaitingSpawn.clear()
		this.AllTechiesMines = []
		this.WaitingExplode.clear()
		this.ItemsMap.clear()
		this.CreepWaveIndex = 0
		this.ThinkerScan.clear()
		this.TickWardPlace = false
		this.WardCaptureTiming = 0
		this.AbilitiesMap.clear()
		this.TeleportsMap.clear()
		this.IceBlastFinal = false
		this.AbilitiesList = []
		this.JungleAttackCreeps = []
		this.TeleportSleepers.clear()
		this.GameSleeper.FullReset()
		this.ModifierTime.clear()
		this.LinearProjectile = []
		this.IceBlastMap.clear()
		this.PudgeHookMap.clear()
		this.lanePaths.Lanes.clear()
		this.SoundAbilities.clear()
		this.RunTimeCheckWard = false
		this.WardDispenserCount.clear()
		this.ParticleSDK.DestroyAll(true)
		this.AbilitiesSpecialUnits.clear()
		this.RunTimeParticles.clear()
		WispModel.Dispose()
		this.AllTechiesMinesMap.clear()
		TemplarAssasinModel.Dispose()
	}
}
