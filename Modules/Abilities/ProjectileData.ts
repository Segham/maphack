import { Ability, grimstroke_dark_artistry, lina_dragon_slave, lion_impale, mirana_arrow, pudge_meat_hook, spirit_breaker_charge_of_darkness, windrunner_powershot } from "wrapper/Imports";

export const AbilitiesPhase: typeof Ability[] = [
	lina_dragon_slave,
	pudge_meat_hook,
	mirana_arrow,
	windrunner_powershot,
	grimstroke_dark_artistry,
	lion_impale,
	spirit_breaker_charge_of_darkness,
]
