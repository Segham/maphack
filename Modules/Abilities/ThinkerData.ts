export const ThinkerParticles: { modName: string, particle?: string}[] = [
	{
		modName: "modifier_invoker_sun_strike",
		particle: "particles/units/heroes/hero_invoker/invoker_sun_strike_team.vpcf",
	},
	{
		modName: "modifier_kunkka_torrent_thinker",
		particle: "particles/units/heroes/hero_kunkka/kunkka_spell_torrent_bubbles_bonus.vpcf",
	},
	{
		modName: "modifier_lina_light_strike_array",
		particle: "particles/units/heroes/hero_lina/lina_spell_light_strike_array_ray_team.vpcf",
	},
	{
		modName: "modifier_monkey_king_spring_thinker",
		particle: "particles/units/heroes/hero_monkey_king/monkey_king_spring_cast.vpcf",
	},
	{
		modName: "modifier_leshrac_split_earth_thinker",
	},
]

export const ParticlesModMarker = new Map<string, [string, number]>([
	[
		"modifier_spirit_breaker_charge_of_darkness_vision",
		["particles/units/heroes/hero_spirit_breaker/spirit_breaker_charge_target_mark.vpcf", 4],
	],
	[
		"modifier_tusk_snowball_target",
		["particles/units/heroes/hero_tusk/tusk_snowball_target.vpcf", 5],
	],
	[
		"modifier_life_stealer_infest_effect",
		["particles/units/heroes/hero_life_stealer/life_stealer_infested_unit_icon.vpcf", 6],
	],
])
