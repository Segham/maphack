export const soundAbilitiesData: {
	nameIcon: string,
	find_owner: string,
	soundCreate: string,
	soundDestroy: string,
	AOERadius: number,
	soundStart?: string,
}[] = [{
	nameIcon: "techies_land_mines",
	find_owner: "npc_dota_hero_techies",
	soundStart: "Hero_Techies.LandMine.Plant",
	soundCreate: "Hero_Techies.LandMine.Priming",
	soundDestroy: "Hero_Techies.LandMine.Detonate",
	AOERadius: 425,
}]
