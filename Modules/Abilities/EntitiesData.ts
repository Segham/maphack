import { IEntitiesData } from "../../data"

export const AbilitiesEntitiesData: IEntitiesData[] = [{
	nameIcon: "nether_ward",
	AbilityName: "pugna_nether_ward",
	AOERadius: abil => abil.AOERadius,
	Duration: abil => abil.MaxDuration,
}, {
	nameIcon: "psionic_trap",
	AbilityName: "templar_assassin_psionic_trap",
	AOERadius: abil => abil.AOERadius,
}, {
	minimap: true,
	nameIcon: "treant_eyes",
	AbilityName: "treant_eyes_in_the_forest",
	AOERadius: abil => abil.AOERadius,
}, {
	nameIcon: "techies_land_mine",
	AbilityName: "techies_land_mines",
	AOERadius: abil => abil.AOERadius,
	Duration: abil => abil.GetSpecialValue("activation_delay"),
}, {
	nameIcon: "venomancer_plague_ward",
	AbilityName: "venomancer_plague_ward",
	AOERadius: abil => abil.AOERadius,
	Duration: abil => abil.GetSpecialValue("duration"),
}, {
	nameIcon: "unit_tombstone",
	AbilityName: "undying_tombstone",
	AOERadius: abil => abil.AOERadius,
	Duration: abil => abil.GetSpecialValue("duration"),

}, {
	nameIcon: "lich_ice_spire",
	AbilityName: "lich_ice_spire",
	AOERadius: abil => abil.GetSpecialValue("aura_radius"),
	Duration: abil => abil.GetSpecialValue("duration"),
}]
