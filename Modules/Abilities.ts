import { MAP_HACK_DATA } from "../data"
import { AbilitiesEnumParticles, AbilitiesMiniMap, AbilitiesState, AbilitiesWorld, WispState } from "../menu"
import { AbilitesModel } from "../Models/Abilities"
import { TemplarAssasinModel } from "../Models/Templar"
import { Color, Entity, EntityManager, GameState, LocalPlayer, npc_dota_hero_morphling, npc_dota_hero_rubick, PARTICLE_RENDER, Unit, Vector3 } from "../wrapper/Imports"
import { AbilitiesParticles } from "./Abilities/AbilitiesData"

export const ABILITIES_RELEASED = (id: number) => {
	if (!AbilitiesMiniMap.value)
		return
	const particle = MAP_HACK_DATA.AbilitiesMap.get(id)
	if (particle === undefined)
		return
	const abils = AbilitiesParticles.find(x => x.path.includes(particle[0]))
	if (abils === undefined || abils.released === undefined)
		return
	MAP_HACK_DATA.AbilitiesMap.delete(id)
	AbilitesModel.RemoveParticles(id)
	MAP_HACK_DATA.GameSleeper.ResetKey(`SLEEP_ABILITY_MAP_HACK_${id}`)
}

export const ABILITIES_DESTROYED = (id: number) => {
	if (!AbilitiesMiniMap.value)
		return

	const particle = MAP_HACK_DATA.AbilitiesMap.get(id)
	if (particle === undefined)
		return

	const data = AbilitiesParticles.find(x => x.path.includes(particle[0]))
	if (data === undefined || (data.replace === undefined && data.delete === undefined))
		return

	MAP_HACK_DATA.AbilitiesMap.delete(id)
	AbilitesModel.RemoveParticles(id)
	MAP_HACK_DATA.GameSleeper.ResetKey(`SLEEP_ABILITY_MAP_HACK_${id}`)
}

export const ABILITIES_TICK = () => {
	if (!AbilitiesState.value)
		return

	const data = [...MAP_HACK_DATA.AbilitiesMap.values()]
	MAP_HACK_DATA.AbilitiesMap.forEach(([path, , ent, , , time, replace], id) => {

		if (ent instanceof Unit && ent.IsIllusion)
			MAP_HACK_DATA.AbilitiesMap.delete(id)

		if (replace !== undefined && data.filter(([x]) => replace !== undefined && x === path).length >= 2)
			MAP_HACK_DATA.AbilitiesMap.delete(id - 1)

		if (time === undefined) {
			MAP_HACK_DATA.AbilitiesMap.delete(id)
			AbilitesModel.RemoveParticles(id)
			return
		}

		if (time >= GameState.RawGameTime)
			return

		MAP_HACK_DATA.AbilitiesMap.delete(id)
		AbilitesModel.RemoveParticles(id)
	})
}

export const ABILITIES_DRAW = () => {
	if (!AbilitiesState.value)
		return

	MAP_HACK_DATA.AbilitiesMap.forEach(([path, abil, ent, start, end, time, , ent2, isSwap], id) => {
		if (start === undefined || time === undefined || start.IsZero() || !(ent instanceof Unit) || ent.IsIllusion)
			return

		AbilitesModel.DrawMiniMapImage(path, abil, ent, start)
		AbilitesModel.DrawBigImage(path, abil, ent, start, id)

		if (end === undefined || end.IsZero())
			return

		AbilitesModel.DrawBigImage(path, abil, (isSwap ? ent2 as Unit : ent), end, id)
	})
}

export const ABILITIES_CREATE = (id: number, path: string, ent: Nullable<Entity | number>) => {
	if (!AbilitiesState.value)
		return

	if (path === "particles/units/heroes/hero_wisp/wisp_tether.vpcf" && WispState.value)
		return

	if (typeof ent === "number")
		return

	if (ent instanceof Entity && TemplarAssasinModel.MeldMap.has(ent.Index)) {
		TemplarAssasinModel.MeldMap.delete(ent.Index)
		MAP_HACK_DATA.ParticleSDK.DestroyByKey(`MAP_HACK_TEMPALR_MELD_${ent.Index}`)
	}

	const data = AbilitiesParticles.find(x => x.path.includes(path))
	if (data === undefined)
		return

	const abil = EntityManager.GetEntitiesByClass(data.abil)[0]
	const abilFirstOwner = EntityManager.GetEntitiesByClass(data.abil).find(x => x.Owner !== undefined && ent?.IsEnemy() && x.Owner === ent)

	if (ent?.IsVisible && !data.excludeIsVisible)
		return

	if (data.showUtilTime !== undefined)
		MAP_HACK_DATA.GameSleeper.Sleep(data.showUtilTime, `SLEEP_ABILITY_MAP_HACK_${id}`)

	MAP_HACK_DATA.AbilitiesMap.set(id, [
		path,
		(ent instanceof npc_dota_hero_rubick || ent instanceof npc_dota_hero_morphling ? abilFirstOwner ?? abil : abil),
		ent ?? abil?.Owner,
		undefined,
		undefined,
		data.timeToShow !== undefined ? (GameState.RawGameTime + data.timeToShow) : 0,
		data.replace,
		undefined,
		data.swapTarget,
	])

	if (data.rectangle !== undefined && AbilitiesWorld.value)
		MAP_HACK_DATA.ParticleSDK.DrawConeFinder(`MAP_HACK_DRAW_REACTAGLE_${id}`, LocalPlayer!.Hero!)
}

export const ABILITIES_UPDATED = (id: number, controlPoint: number, position: Vector3) => {
	if (!AbilitiesState.value)
		return

	const particle = MAP_HACK_DATA.AbilitiesMap.get(id)
	if (particle === undefined)
		return

	const data = AbilitiesParticles.find(x => x.path.includes(particle[0]))
	if (data === undefined)
		return

	if (particle[2] instanceof Unit && !data.excludeIsVisible && (!particle[2].IsValid || particle[2].IsIllusion || (!particle[2].IsEnemy() && particle[2].IsVisible))) {
		MAP_HACK_DATA.AbilitiesMap.delete(id)
		AbilitesModel.RemoveParticles(id)
		return
	}

	if (data.ctrPoint !== undefined && data.ctrPoint === controlPoint) {
		const change_pos = data.noSetZ === undefined
			? position.SetZ(350)
			: position
		MAP_HACK_DATA.AbilitiesMap.set(id, [
			particle[0],
			particle[1],
			particle[2],
			change_pos,
			particle[4],
			particle[5],
			particle[6],
			particle[7],
		])

		if (data.range !== undefined && AbilitiesWorld.value && AbilitiesEnumParticles.selected_id !== 0) {
			const mode_particles = AbilitiesEnumParticles.selected_id as PARTICLE_RENDER - 1
			MAP_HACK_DATA.ParticleSDK.DrawCircle(`MAP_HACK_DRAW_RANGE_${id}`, LocalPlayer!.Hero!, particle[1].AOERadius, {
				Position: position,
				RenderStyle: mode_particles,
			})
		}

		if (data.rectangle !== undefined && AbilitiesWorld.value)
			MAP_HACK_DATA.ParticleSDK.SetConstrolPointsByKey(`MAP_HACK_DRAW_REACTAGLE_${id}`, [1, position])
	}

	if (data.ctrPointEnd !== undefined && data.ctrPointEnd === controlPoint) {
		const change_pos2 = data.noSetZ === undefined
			? position.SetZ(350)
			: position

		MAP_HACK_DATA.AbilitiesMap.set(id, [
			particle[0],
			particle[1],
			particle[2],
			particle[3],
			change_pos2,
			particle[5],
			particle[6],
			particle[7],
		])

		if (data.rangeTimer !== undefined && AbilitiesWorld.value)
			MAP_HACK_DATA.ParticleSDK.DrawTimerRange(`MAP_HACK_DRAW_RANGE_TIMER_${id}`, LocalPlayer!.Hero!, {
				Position: position,
				Width: particle[1]?.AOERadius ?? 300,
			})

		if (data.rectangle !== undefined && AbilitiesWorld.value) {
			let aoe = 125
			if (particle[1]?.AOERadius !== 0)
				aoe = particle[1]?.AOERadius ?? 125

			MAP_HACK_DATA.ParticleSDK.SetConstrolPointsByKey(`MAP_HACK_DRAW_REACTAGLE_${id}`,
				[2, position],
				[3, new Vector3(aoe, aoe, 0)],
				[4, Color.Red],
			)
		}

		if (data.rangeEnd === undefined || !AbilitiesWorld.value || AbilitiesEnumParticles.selected_id === 0)
			return

		const mode_particles = AbilitiesEnumParticles.selected_id as PARTICLE_RENDER - 1
		MAP_HACK_DATA.ParticleSDK.DrawCircle(`MAP_HACK_DRAW_RANGE_${id}`, LocalPlayer!.Hero!, particle[1].AOERadius, {
			Position: position,
			RenderStyle: mode_particles,
		})
	}
}

export const ABILITIES_UPDATED_ENT = (id: number, controlPoint: number, entity: number | Entity, position: Vector3) => {
	if (!AbilitiesState.value)
		return

	if (entity instanceof Entity && TemplarAssasinModel.MeldMap.has(entity.Index)) {
		TemplarAssasinModel.MeldMap.delete(entity.Index)
		MAP_HACK_DATA.ParticleSDK.DestroyByKey(`MAP_HACK_TEMPALR_MELD_${entity.Index}`)
	}

	const particle = MAP_HACK_DATA.AbilitiesMap.get(id)
	if (particle === undefined)
		return

	const data = AbilitiesParticles.find(x => x.path.includes(particle[0]))
	if (data === undefined)
		return

	if (entity instanceof Unit && !data.excludeIsVisible && (!entity.IsValid || entity.IsIllusion || !entity.IsEnemy() || entity.IsVisible)) {
		MAP_HACK_DATA.AbilitiesMap.delete(id)
		AbilitesModel.RemoveParticles(id)
		return
	}

	if (data.ctrPoint !== undefined && data.ctrPoint === controlPoint) {

		const change_pos = data.noSetZ === undefined
			? position.SetZ(350)
			: position

		const swapEnt = data.swapTarget
			? entity
			: particle[2] ?? entity ?? particle[1]?.Owner ?? undefined

		MAP_HACK_DATA.AbilitiesMap.set(id, [
			particle[0],
			particle[1],
			swapEnt,
			change_pos,
			particle[4],
			particle[5],
			particle[6],
			particle[7],
		])

		if (data.range !== undefined && AbilitiesWorld.value && AbilitiesEnumParticles.selected_id !== 0) {
			const mode_particles = AbilitiesEnumParticles.selected_id as PARTICLE_RENDER - 1
			MAP_HACK_DATA.ParticleSDK.DrawCircle(`MAP_HACK_DRAW_RANGE_${id}`, LocalPlayer!.Hero!, particle[1].AOERadius, {
				Position: position,
				RenderStyle: mode_particles,
			})
		}

		if (data.rectangle !== undefined && AbilitiesWorld.value)
			MAP_HACK_DATA.ParticleSDK.SetConstrolPointsByKey(`MAP_HACK_DRAW_REACTAGLE_${id}`, [1, position])
	}

	if (data.ctrPointEnd !== undefined && data.ctrPointEnd === controlPoint) {

		const change_pos2 = data.noSetZ === undefined
			? position.SetZ(350)
			: position

		const ent = entity as Entity

		MAP_HACK_DATA.AbilitiesMap.set(id, [
			particle[0],
			particle[1],
			entity ?? particle[1].Owner,
			particle[3],
			change_pos2,
			particle[5],
			particle[6],
			ent ?? particle[1].Owner,
		])

		if (data.range !== undefined && AbilitiesWorld.value && AbilitiesEnumParticles.selected_id !== 0) {
			const mode_particles = AbilitiesEnumParticles.selected_id as PARTICLE_RENDER - 1
			MAP_HACK_DATA.ParticleSDK.DrawCircle(`MAP_HACK_DRAW_RANGE_${id}`, LocalPlayer!.Hero!, particle[1].AOERadius, {
				Position: position,
				RenderStyle: mode_particles,
			})
		}

		if (data.rangeTimer !== undefined && AbilitiesWorld.value)
			MAP_HACK_DATA.ParticleSDK.DrawTimerRange(`MAP_HACK_DRAW_RANGE_TIMER_${id}`, LocalPlayer!.Hero!, {
				Position: position,
				Width: particle[1]?.AOERadius ?? 300,
			})

		if (data.rangeEnd !== undefined && AbilitiesWorld.value && AbilitiesEnumParticles.selected_id !== 0) {
			const mode_particles = AbilitiesEnumParticles.selected_id as PARTICLE_RENDER - 1
			MAP_HACK_DATA.ParticleSDK.DrawCircle(`MAP_HACK_DRAW_RANGE_${id}`, LocalPlayer!.Hero!, particle[1].AOERadius, {
				Position: position,
				RenderStyle: mode_particles,
			})
		}
	}
}
