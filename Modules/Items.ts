import { MAP_HACK_DATA } from "../data"
import { AbilitiesEnumParticles, AbilitiesMiniMap } from "../menu"
import { Color, Entity, EntityManager, GameState, Item, item_smoke_of_deceit, LocalPlayer, Menu, MinimapSDK, RendererSDK, Vector2, Vector3 } from "../wrapper/Imports"
import { EntityX, PathX, RectangleDraw, RectangleX } from "../X-Core/Imports"
import { ItemsParticles } from "./Abilities/ItemsData"

function DrawBigImage(ent: Nullable<Entity>, item: Item, position: Vector3) {

	const screen = RendererSDK.WorldToScreen(position)
	if (screen === undefined)
		return

	const Size = 45 * (RendererSDK.WindowSize.y / 1080)
	const pos = new RectangleX(screen, new Vector2(Size, Size))
	if (pos.IsZero())
		return

	if (ent === undefined) {
		RectangleDraw.Image(PathX.DOTAItems(item.Name), pos, Color.White, 0)
		RectangleDraw.Image(PathX.Images.buff_outline, pos, Color.Red.SetA(230))
		return
	}

	if (ent.IsVisible)
		return

	RectangleDraw.Image(PathX.Heroes(ent.Name, false), pos, Color.White, 0)
	RectangleDraw.Image(PathX.Images.buff_outline, pos, Color.Red.SetA(230))
	const abilPos = pos.Clone().MultiplySizeFX(0.5)
	abilPos.X += abilPos.Width * 0.8
	abilPos.Y += abilPos.Height * 0.6
	const outline2 = abilPos.Clone().MultiplySizeFX(1.2)

	RectangleDraw.Image(
		PathX.DOTAItems(item.Name),
		abilPos,
		Color.White.SetA(230), 0,
	)

	RectangleDraw.Image(
		PathX.Images.buff_outline,
		outline2,
		Color.Red.SetA(230),
	)
}

export const ITEMS_CREATE = (id: number, path: string, ent: Nullable<Entity | number>) => {
	if (!AbilitiesMiniMap.value || typeof ent === "number")
		return
	if (path === "particles/items_fx/blink_dagger_end.vpcf") {
		MAP_HACK_DATA.ItemsMap.forEach(([p, , , c, d, e]) => {
			if (p !== "particles/items_fx/blink_dagger_start.vpcf")
				return
			MAP_HACK_DATA.ItemsMap.set(id, [path, ent, GameState.RawGameTime + 2, c, d, e])
		})
		return
	}

	const data = ItemsParticles.find(x => x.path === path)
	if (data === undefined)
		return

	const item = EntityManager.GetEntitiesByClass(data.item).find(x => x.Owner !== undefined
		&& x.Owner.IsEnemy())

	MAP_HACK_DATA.ItemsMap.set(id, [
		path,
		ent,
		GameState.RawGameTime + data.timeToShow,
		data.ctrPoint,
		undefined,
		item,
	])
}

export const ITEMS_UPDATED = (id: number, controlPoint: number, position: Vector3) => {
	if (!AbilitiesMiniMap.value)
		return

	const findID = MAP_HACK_DATA.ItemsMap.get(id)
	if (findID === undefined || controlPoint !== findID[3])
		return

	const data = ItemsParticles.find(x => x.path === findID[0])
	if (data === undefined)
		return

	if (EntityX.AllyHeroes.some(x => x.Position.Distance2D(position) <= 100)) {
		MAP_HACK_DATA.ItemsMap.delete(id)
		return
	}

	if (EntityX.AllyHeroes.some(x => x.HasBuffByName("modifier_smoke_of_deceit") && x.Position.Distance2D(position) <= 64)) {
		MAP_HACK_DATA.ItemsMap.delete(id)
		return
	}

	if (data.range && AbilitiesEnumParticles.selected_id !== 0)
		MAP_HACK_DATA.ParticleSDK.DrawCircle(`ITEMS_PARTICLES_RADIUS_${id}`, LocalPlayer!.Hero!, findID[5]?.AOERadius ?? data.range, {
			Position: position,
			RenderStyle: AbilitiesEnumParticles.selected_id - 1,
		})

	MAP_HACK_DATA.ItemsMap.set(id, [findID[0], findID[1], findID[2], controlPoint, position, findID[5]])
}

export const ITEMS_UPDATED_ENT = (id: number, controlPoint: number, ent: Entity | number, position: Vector3) => {
	if (!AbilitiesMiniMap.value || typeof ent === "number")
		return

	const findID = MAP_HACK_DATA.ItemsMap.get(id)
	if (findID === undefined || controlPoint !== findID[3])
		return

	MAP_HACK_DATA.ItemsMap.set(id, [findID[0], ent, findID[2], controlPoint, position, findID[5]])
}

export const ITEMS_TICK = () => {
	if (!AbilitiesMiniMap.value || MAP_HACK_DATA.ItemsMap.size === 0)
		return

	MAP_HACK_DATA.ItemsMap.forEach(([, ent, time, , position], id) => {

		if (time > GameState.RawGameTime && position !== undefined && !position.IsZero())
			return

		if (ent !== undefined && !ent.IsAlive) {
			if (MAP_HACK_DATA.ParticleSDK.AllParticles.has(`ITEMS_PARTICLES_RADIUS_${id}`))
				MAP_HACK_DATA.ParticleSDK.DestroyByKey(`ITEMS_PARTICLES_RADIUS_${id}`)
			MAP_HACK_DATA.ItemsMap.delete(id)
			return
		}

		if (MAP_HACK_DATA.ParticleSDK.AllParticles.has(`ITEMS_PARTICLES_RADIUS_${id}`))
			MAP_HACK_DATA.ParticleSDK.DestroyByKey(`ITEMS_PARTICLES_RADIUS_${id}`)

		MAP_HACK_DATA.ItemsMap.delete(id)
	})
}

export const ITEMS_DRAW = () => {
	if (!AbilitiesMiniMap.value || MAP_HACK_DATA.ItemsMap.size === 0)
		return

	MAP_HACK_DATA.ItemsMap.forEach(([, ent, , , position, item], id) => {
		if (position === undefined || position.IsZero() || item === undefined)
			return

		if (item === undefined || !(item instanceof item_smoke_of_deceit)) {
			DrawBigImage(ent, item, position)
			return
		}

		if ((GameState.RawGameTime / 60) <= 14 && !MAP_HACK_DATA.GameSleeper.Sleeping(`ITEM_MAP_HACK_${id}`)) {
			MinimapSDK.DrawPing(position, Color.Aqua, (GameState.RawGameTime + 5), id)
			MAP_HACK_DATA.GameSleeper.Sleep(2000, `ITEM_MAP_HACK_${id}`)
		}

		if ((GameState.RawGameTime / 60) >= 15)
			MinimapSDK.DrawIcon("ping", position, 1000, Color.Red)

		RendererSDK.TextAroundMouse(Menu.Localization.Localize("Enemy team used smoke"), false, Color.White, "Calibri", 20)
		const screen = RendererSDK.WorldToScreen(position)
		if (screen === undefined)
			return

		const Size = 45 * (RendererSDK.WindowSize.y / 1080)
		const pos = new RectangleX(screen, new Vector2(Size, Size))
		RectangleDraw.Image(PathX.DOTAItems("item_smoke_of_deceit"), pos, Color.White, 0)
		RectangleDraw.Image(PathX.Images.buff_outline, pos, Color.Red.SetA(230))
	})
}
