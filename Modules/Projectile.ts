import { ancient_apparition_ice_blast, ArrayExtensions, Color, Entity, EntityManager, GameState, Hero, LinearProjectile, lion_impale, LocalPlayer, MinimapSDK, ParticleAttachment_t, RendererSDK, spirit_breaker_charge_of_darkness, Vector2, Vector3 } from "wrapper/Imports"
import { MAP_HACK_DATA } from "../data"
import { AbilitiesMiniMap, AbilitiesScaleMiniMap, AbilitiesScaleWorld, AbilitiesState, AbilitiesWorld } from "../menu"
import { VectorX } from "../Service/Vector"
import { PathX, PlayerX, RectangleDraw, RectangleX } from "../X-Core/Imports"

function DestroyDirectional(key: any) {
	MAP_HACK_DATA.ParticleSDK.DestroyByKey(`MAP_HACK_RECT_1_${key}`)
	MAP_HACK_DATA.ParticleSDK.DestroyByKey(`MAP_HACK_DIRECT_2_${key}`)
	MAP_HACK_DATA.ParticleSDK.DestroyByKey(`MAP_HACK_DIRECT_3_${key}`)
}

function CreateDirectional(key: any, is_aoe?: boolean) {
	if (is_aoe)
		MAP_HACK_DATA.ParticleSDK.DrawConeFinder(`MAP_HACK_RECT_1_${key}`, LocalPlayer!.Hero!)

	MAP_HACK_DATA.ParticleSDK.AddOrUpdate(
		`MAP_HACK_DIRECT_2_${key}`,
		"particles/ui_mouseactions/range_finder_directional_b.vpcf",
		ParticleAttachment_t.PATTACH_CUSTOMORIGIN,
		undefined,
	)
	MAP_HACK_DATA.ParticleSDK.AddOrUpdate(
		`MAP_HACK_DIRECT_3_${key}`,
		"particles/ui_mouseactions/range_finder_directional_c.vpcf",
		ParticleAttachment_t.PATTACH_CUSTOMORIGIN,
		undefined,
	)
}

function IceBLastRange(key: string, pos: Vector3, radius: number) {
	MAP_HACK_DATA.ParticleSDK.AddOrUpdate(key,
		"particles/econ/items/ancient_apparition/aa_blast_ti_5/ancient_apparition_ice_blast_marker_ti5.vpcf",
		ParticleAttachment_t.PATTACH_ABSORIGIN,
		LocalPlayer!.Hero!,
		[0, pos],
		[1, new Vector3(radius, 1, 1)],
	)
	MAP_HACK_DATA.ParticleSDK.DrawTimerRange(key + "_timer", LocalPlayer!.Hero!, {
		Position: pos,
		Width: radius,
	})
}

function IceBLastLine(key: string, start: Vector3, end: Vector3, size: number, alpha: number, color: Color) {
	MAP_HACK_DATA.ParticleSDK.DrawConeFinder(key, LocalPlayer!.Hero!, {
		PositionStart: start,
		PositionEnd: end,
		Width: size,
		Color: color.SetA(alpha),
	})
}

// TODO: improve this shit code on setctrpoints
function DrawIceBlast() {
	if (MAP_HACK_DATA.IceBlastMap.size <= 0)
		return
	MAP_HACK_DATA.IceBlastMap.forEach(([, vec, vec2, vec3]) => {
		if (!vec2.IsValid || vec3.x === 0 || !vec.IsValid)
			return
		const ctr5 = VectorX.Multiply(vec3.x, vec2)
		const endPosition = vec.Add(ctr5)
		const num = vec.Distance(endPosition) / 20
		IceBLastRange("IceBlastEnd", endPosition, 275 + num)
		IceBLastLine("IceBlast", vec, endPosition, 300, 185, Color.Red)
		const rawGameTime = GameState.RawGameTime
		const speed = vec.Distance(vec.Add(vec2).MultiplyScalar(1.04))
		const position = vec.Extend(endPosition, (GameState.RawGameTime - rawGameTime) * speed)
		IceBLastRange("IceBlastPosition", position, 100)
		if (!MAP_HACK_DATA.IceBlastFinal)
			return
		MAP_HACK_DATA.ParticleSDK.DestroyByKey("IceBlastEnd")
		MAP_HACK_DATA.ParticleSDK.DestroyByKey("IceBlast")
		MAP_HACK_DATA.ParticleSDK.DestroyByKey("IceBlastPosition")
		MAP_HACK_DATA.ParticleSDK.DestroyByKey("IceBlastEnd_timer")
		MAP_HACK_DATA.ParticleSDK.DestroyByKey("IceBlastPosition_timer")
	})
}

function SetControlPoints(key: any, start: Vector3, end: Vector3, aoe?: number, color?: Color) {
	if (aoe) {
		MAP_HACK_DATA.ParticleSDK.SetConstrolPointsByKey(`MAP_HACK_RECT_1_${key}`,
			[1, start],
			[2, end],
			[3, new Vector3(aoe ?? 125, aoe ?? 125, aoe ?? 125)],
			[4, color ?? Color.Red],
		)
	}
	MAP_HACK_DATA.ParticleSDK.SetConstrolPointsByKey(`MAP_HACK_DIRECT_2_${key}`,
		[1, start],
		[2, end],
	)
	MAP_HACK_DATA.ParticleSDK.SetConstrolPointsByKey(`MAP_HACK_DIRECT_3_${key}`,
		[0, start],
		[2, end],
	)
}

export const ABILITIES_LINEAR_PROJECTILE_TICK = () => {
	if (!AbilitiesState.value || !AbilitiesWorld.value)
		return

	DrawIceBlast()

	MAP_HACK_DATA.AbilitiesList.forEach(abil => {

		if (abil === undefined || abil instanceof spirit_breaker_charge_of_darkness)
			return

		const owner = abil.Owner
		if (owner === undefined || !owner.IsEnemy())
			return

		if (abil.IsInAbilityPhase || (owner.IsChanneling && abil.Name === "windrunner_powershot")) {
			CreateDirectional(abil.Index, abil.AOERadius !== 0)

			let castRange = abil.CastRange
			if (abil instanceof lion_impale)
				castRange += abil.GetSpecialValue("length_buffer")

			SetControlPoints(abil.Index, owner.Position, owner.InFront(castRange), abil.AOERadius)
		} else
			DestroyDirectional(abil.Index)
	})

	MAP_HACK_DATA.PudgeHookMap.forEach((par, index) => {
		const end_pos = par[2]
		const start_pos = par[1]
		if (par[0] !== "particles/units/heroes/hero_pudge/pudge_meathook.vpcf" || !par[1].IsValid || !par[2].IsValid)
			return

		const calc_pos = start_pos.Extend(end_pos, (GameState.RawGameTime - par[3]) * par[4])

		SetControlPoints(index, start_pos, end_pos, 100)
		SetControlPoints(`${index}_predict`, start_pos, calc_pos, 100, Color.Aqua)

		if (start_pos.Distance2D(calc_pos) > start_pos.Distance2D(end_pos)) {
			DestroyDirectional(index)
			DestroyDirectional(`${index}_predict`)
			MAP_HACK_DATA.PudgeHookMap.delete(index)
		}
	})

	MAP_HACK_DATA.LinearProjectile.forEach(proj =>
		SetControlPoints(proj.ID, proj.Position, proj.TargetLoc, proj.FowRadius))
}

export const ABILITIES_LINEAR_PROJECTILE_DRAW = () => {
	if (!AbilitiesState.value)
		return

	MAP_HACK_DATA.AbilitiesList.forEach(abil => {
		if (abil === undefined || !(abil instanceof spirit_breaker_charge_of_darkness))
			return
		const owner = abil.Owner
		if (owner === undefined || !owner.IsEnemy() || owner.IsVisible || abil.CurrentProjectile === undefined)
			return
		const pos = abil.CurrentProjectile.Position

		if (AbilitiesMiniMap.value)
			MinimapSDK.DrawIcon(`heroicon_${owner.Name}`, pos, AbilitiesScaleMiniMap.value * 100, Color.White)

		const screen = RendererSDK.WorldToScreen(pos)
		if (screen === undefined || !AbilitiesWorld.value)
			return
		const Size = AbilitiesScaleWorld.value * (RendererSDK.WindowSize.y / 1080)
		const position = new RectangleX(screen, new Vector2(Size, Size))
		if (position.IsZero())
			return

		const pcolor = (PlayerX.ColorX.get((owner as Hero).PlayerID) ?? Color.Red).SetA(230)
		const outline = position.Clone().MultiplySizeFX(1.2)
		RectangleDraw.Image(PathX.Images.buff_outline, outline, pcolor)
		RectangleDraw.Image(PathX.Heroes(owner.Name, false), position, Color.White, 0)

		const abilPos = position.Clone().MultiplySizeFX(0.5)
		abilPos.X += abilPos.Width * 0.8
		abilPos.Y += abilPos.Height * 0.6
		const outline2 = abilPos.Clone().MultiplySizeFX(1.2)
		RectangleDraw.Image(PathX.DOTAAbilities(abil.Name), abilPos, Color.White.SetA(230), 0)
		RectangleDraw.Image(PathX.Images.buff_outline, outline2, Color.Red.SetA(230))
	})
}

export const ABILITIES_LINEAR_PROJECTILE_CREATE = (proj: LinearProjectile) => {
	if (!AbilitiesState.value || !(proj.Source instanceof Entity) || !proj.Source.IsEnemy())
		return

	if (proj.FowRadius === 0
		|| proj.ParticlePath === "particles/units/heroes/hero_tinker/tinker_machine.vpcf"
		|| proj.ParticlePath === "particles/units/heroes/hero_weaver/weaver_swarm_projectile.vpcf")
		return

	CreateDirectional(proj.ID, proj.FowRadius !== 0)
	MAP_HACK_DATA.LinearProjectile.push(proj)
}

export const ABILITIES_LINEAR_PROJECTILE_DESTROY = (proj: LinearProjectile) => {
	if (!AbilitiesState.value)
		return
	DestroyDirectional(proj.ID)
	ArrayExtensions.arrayRemove(MAP_HACK_DATA.LinearProjectile, proj)
}

export const ABILITIES_PARTICLES_PROJ_CREATE = (id: number, path: string) => {
	if (!AbilitiesState.value || !AbilitiesWorld.value)
		return

	if (path.indexOf("ancient_apparition_ice_blast_final") !== -1) {

		const abil = EntityManager.GetEntitiesByClass(ancient_apparition_ice_blast)
			.find(x => x.Owner !== undefined && x.Owner.IsEnemy())

		if (abil === undefined)
			return

		MAP_HACK_DATA.IceBlastFinal = false
		MAP_HACK_DATA.IceBlastMap.set(id, [0, new Vector3(), new Vector3(), new Vector3()])
	}

	if (path.indexOf("ancient_apparition_ice_blast_explode") !== -1)
		MAP_HACK_DATA.IceBlastFinal = true

	if (path !== "particles/units/heroes/hero_pudge/pudge_meathook.vpcf")
		return

	MAP_HACK_DATA.PudgeHookMap.set(id, [path, new Vector3().Invalidate(), new Vector3().Invalidate(), GameState.RawGameTime, 1450 /* hook_speed */])
	CreateDirectional(id, true)
	CreateDirectional(`${id}_predict`, true)
}

export const ABILITIES_PARTICLES_PROJ_UPDATE = (id: number, controlPoint: number, position: Vector3) => {
	if (!AbilitiesState.value || !AbilitiesWorld.value)
		return

	const IceBlast = MAP_HACK_DATA.IceBlastMap.get(id)
	if (IceBlast !== undefined) {
		if (controlPoint === 0)
			IceBlast[1] = position
		if (controlPoint === 1)
			IceBlast[2] = position
		if (controlPoint === 5)
			IceBlast[3] = position
	}

	const hook = MAP_HACK_DATA.PudgeHookMap.get(id)
	if (hook === undefined || controlPoint !== 1)
		return

	hook[2] = position
}

export const ABILITIES_PARTICLES_PROJ_UPDATE_ENT = (id: number, controlPoint: number, ent: number | Entity, position: Vector3) => {
	if (!AbilitiesState.value || !AbilitiesWorld.value || ent === undefined || typeof ent === "number")
		return
	const part = MAP_HACK_DATA.PudgeHookMap.get(id)
	if (part === undefined || !ent.IsEnemy()) {
		MAP_HACK_DATA.PudgeHookMap.delete(id)
		DestroyDirectional(id)
		DestroyDirectional(`${id}_predict`)
		return
	}
	if (controlPoint === 0)
		part[1] = position.Add((ent instanceof Entity ? ent.Forward : new Vector3()).MultiplyScalar(150))
	else if (controlPoint === 1) {
		DestroyDirectional(id)
		DestroyDirectional(`${id}_predict`)
		MAP_HACK_DATA.PudgeHookMap.delete(id)
	}
}

export const ABILITIES_PARTICLES_PROJ_DESTROY = (id: number) => {
	if (!AbilitiesState.value)
		return

	DestroyDirectional(id)
	DestroyDirectional(`${id}_predict`)

	if (MAP_HACK_DATA.PudgeHookMap.has(id))
		MAP_HACK_DATA.PudgeHookMap.delete(id)
}
