import { MAP_HACK_DATA } from "../data"
import { JungleCreepMapGold, JungleCreepMapState } from "../menu"
import { Color, Creep, Hero, Input, RendererSDK, Vector2, VKeys } from "../wrapper/Imports"
import { EntityX, PathX } from "../X-Core/Imports"

export const MAP_HACK_CAMP_TICK = () => {
	if (!JungleCreepMapState.value)
		return
	MAP_HACK_DATA.Units = [
		...EntityX.AllyHeroes,
		...EntityX.EnemyHeroes,
		...EntityX.AllyCreeps,
		...EntityX.EnemyCreeps,
	].filter(unit => (unit instanceof Hero || (unit instanceof Creep && !unit.IsLaneCreep && !unit.IsNeutral))
		&& unit.IsAlive
		&& unit.IsVisible,
	)
}

export const MAP_HACK_CAMP_DRAW = () => {
	if (!JungleCreepMapState.value)
		return

	EntityX.Camps.forEach(spawner => {
		const screen_position = RendererSDK.WorldToScreen(spawner.Position)
		if (screen_position === undefined)
			return

		if (MAP_HACK_DATA.Units.find(x => x.IsInRange(spawner.Position, 300)) !== undefined && !Input.IsKeyDown(VKeys.MENU))
			return

		const pos = screen_position
			.SubtractScalarY(100).Clone()
		const Size = new Vector2(20 * 1.5, 20) // TODO resize

		const Bounty = spawner.Creeps.map((creep, index) => {
			const pos_cur = new Vector2(pos.x, pos.y + (index * 21)) // TODO resize
			RendererSDK.Image(PathX.Heroes(creep.Name, false), pos_cur, -1, Size, !creep.IsSpawned ? RendererSDK.GrayScale : Color.White)
			RendererSDK.GrayScale[3] = RendererSDK.GrayScale[8] = RendererSDK.GrayScale[13] = 0
			RendererSDK.GrayScale[18] = 1
			RendererSDK.OutlinedRect(pos_cur, Size, 1, creep.IsSpawned ? Color.Green.SetA(160) : Color.Red.SetA(160))
			return creep.IsWaitingToSpawn ? 0 : Math.round(((creep.GoldBountyMin + (creep.GoldBountyMax)) / 2))
		}).reduce((prev, curr) => prev + curr, 0)

		if (Bounty <= 0 || !JungleCreepMapGold.value)
			return

		RendererSDK.Image(PathX.Images.gold_large,
			pos.Clone().SubtractScalarY(5).AddScalarX(30),
			-1,
			new Vector2(20, 20),

			)
		RendererSDK.Text(
			`${Bounty.toString()}≈`,
			pos.Clone().AddScalarX(30).AddScalarY(15),
			Color.White, "Calibri",
			18,
		)
	})
}
