import { PathX, RectangleDraw, RectangleX } from "gitlab.com/FNT_Rework/x-core/Imports"
import { Color, LocalPlayer, MinimapSDK, Modifier, NotificationsSDK, RendererSDK, Team, Vector2 } from "wrapper/Imports"
import { MAP_HACK_DATA } from "../data"
import { ScanMiniMap, ScanScaleMiniMap, ScanScaleWorld, ScanSoundVolunme, ScanState, ScanWorld } from "../menu"
import { NotificationScan } from "../Models/Scan"

export const IsValidThinker = (modifier: Modifier) =>  {
	return modifier.Name === "modifier_radar_thinker"
		&& modifier.Parent !== undefined
		&& modifier.Parent.IsEnemy()
}

export const SCAN_MODIFIER_DRAW = () => {
	if (!ScanState.value)
		return
	MAP_HACK_DATA.ThinkerScan.forEach(modifier => {
		if (ScanMiniMap.value)
			MinimapSDK.DrawIcon("ping_teleporting", modifier.Parent!.Position, ScanScaleMiniMap.value * 100, Color.Red)
		if (!ScanWorld.value)
			return
		const screen = RendererSDK.WorldToScreen(modifier.Parent!.Position)
		if (screen === undefined)
			return
		const Size = (ScanScaleWorld.value * (RendererSDK.WindowSize.y / 1080))
		const position = new RectangleX(screen, new Vector2(Size, Size))
		if (position.IsZero())
			return
		position.Position.SubtractScalarForThis(20)
		RectangleDraw.Image(PathX.Images.icon_scan, position, Color.White)
		const outline = position.Clone().MultiplySizeFX(1.2)
		RectangleDraw.Image(PathX.Images.buff_outline, outline, Color.Red)
		RectangleDraw.Arc(-90, modifier.RemainingTime, modifier.Duration, true, position, Color.Black)
	})
}

export const SCAN_MODIFIER_CREATE = (modifier: Modifier) => {
	if (!IsValidThinker(modifier) || !ScanState.value)
		return
	MAP_HACK_DATA.ParticleSDK.DrawCircle(modifier.Parent!.Index, LocalPlayer!.Hero!, 900, {
		Position: modifier.Parent!.Position,
		RenderStyle: 2,
	})

	// RendererSDK.EmitChatEvent(
	// 	DOTA_CHAT_MESSAGE.CHAT_MESSAGE_SCAN_USED,
	// 	modifier.Parent!.Team,
	// 	0,
	// 	0,
	// 	0,
	// 	0,
	// 	0,
	// 	0,
	// 	0,
	// 	0,
	// )

	const soundName = LocalPlayer!.Hero!.Team !== Team.Radiant
		? "sounds/vo/announcer/announcer_scan_rad_01.vsnd_c"
		: "sounds/vo/announcer/announcer_scan_dire_01.vsnd_c"

	NotificationsSDK.Push(new NotificationScan(soundName, (ScanSoundVolunme.value / 100)))
	MAP_HACK_DATA.ThinkerScan.set(modifier.Parent!.Index, modifier)
}

export const SCAN_MODIFIER_DESTROY = (modifier: Modifier) => {
	if (!IsValidThinker(modifier) || !ScanState.value)
		return
	MAP_HACK_DATA.ParticleSDK.DestroyByKey(modifier.Parent!.Index)
	MAP_HACK_DATA.ThinkerScan.delete(modifier.Parent!.Index)
}
