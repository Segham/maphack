import { EntityX, PathX, PlayerX, RectangleDraw, RectangleX } from "gitlab.com/FNT_Rework/x-core/Imports";
import { Color, Entity, GameState, Hero, LocalPlayer, MinimapSDK, RendererSDK, Unit, Vector2, Vector3 } from "wrapper/Imports";
import { MAP_HACK_DATA } from "../data";
import { AbilitiesEnumParticles, AbilitiesMiniMap, AbilitiesScaleWorld, AbilitiesState, AbilitiesWorld } from "../menu";
import { AbilitiesEntitiesData } from "./Abilities/EntitiesData"
import { soundAbilitiesData } from "./Abilities/TechiesData"

function CreateSoundMine(entIndex: Nullable<number>, pos: Vector3, endTime: number) {
	const mine = soundAbilitiesData.find(x => x.nameIcon)
	if (mine === undefined || entIndex === undefined)
		return
	if (!MAP_HACK_DATA.RunTimeParticles.has(entIndex) && AbilitiesEnumParticles.selected_id !== 0) {
		MAP_HACK_DATA.RunTimeParticles.set(entIndex,
			MAP_HACK_DATA.ParticleSDK.DrawCircle(`MAP_HACK_ABILITIES_ENTITIES_${entIndex}`, LocalPlayer!.Hero!, mine.AOERadius, {
			Position: pos,
			RenderStyle: AbilitiesEnumParticles.selected_id - 1,
		}))
	}

	const screen = RendererSDK.WorldToScreen(pos)
	if (screen === undefined)
		return

	const Size = ((AbilitiesScaleWorld.value - 5) * (RendererSDK.WindowSize.y / 1080))
	const position = new RectangleX(screen, new Vector2(Size, Size))
	if (position.IsZero())
		return

	const sec = endTime - GameState.RawGameTime + 1.67
	const findPlayer = EntityX.AllyUnits.find(x => x.Position.Distance2D(pos) <= mine.AOERadius)
	position.Position.SubtractScalarForThis(20)
	if (findPlayer !== undefined) {
		RendererSDK.Text(sec.toFixed(1), position.Position.AddScalarForThis(10), Color.White, "Calibri", position.Width / 2)
		return
	}
	RectangleDraw.Image(PathX.DOTAAbilities(mine.nameIcon), position, Color.White, 0)
	const outline = position.Clone().MultiplySizeFX(1.2)
	RectangleDraw.Image(PathX.Images.buff_outline, outline, Color.Red)
	const unitlPos = position.Clone().MultiplySizeFX(0.5)
	unitlPos.X += unitlPos.Width * 0.8
	unitlPos.Y += unitlPos.Height * 0.6
	const outline2 = unitlPos.Clone().MultiplySizeFX(1.2)
	RectangleDraw.Image(PathX.Heroes("npc_dota_hero_techies"), unitlPos, Color.White.SetA(230), 0)
	RectangleDraw.Image(PathX.Images.buff_outline, outline2, Color.Red)

}

export const MAP_HACK_ABILITIES_ENTITIES_DRAW = () => {
	if (!AbilitiesState.value)
		return

	MAP_HACK_DATA.AbilitiesSpecialUnits.forEach(([data, unit, endTime, entIndex]) => {

		if (!(unit instanceof Unit)) {
			CreateSoundMine(entIndex, unit, endTime)
			return
		}
		const Owner = unit.Owner ?? unit.OwnerEntity

		if (!(Owner instanceof Unit)) {
			MAP_HACK_DATA.AbilitiesSpecialUnits.delete(unit.Index)
			return
		}

		const abil = Owner.GetAbilityByName(data.AbilityName)
		if (abil === undefined)
			return

		if (data.minimap && AbilitiesMiniMap.value)
			MinimapSDK.DrawIcon("treant_ult", unit.Position, 400, Color.Red)

		if (!AbilitiesWorld.value)
			return

		const range = data.AOERadius(abil) <= 0
			? unit.DayVision
			: abil.AOERadius

		if (range !== 0 && !MAP_HACK_DATA.RunTimeParticles.has(unit.Index) && AbilitiesEnumParticles.selected_id !== 0) {
			MAP_HACK_DATA.RunTimeParticles.set(unit.Index, MAP_HACK_DATA.ParticleSDK.DrawCircle(`MAP_HACK_ABILITIES_ENTITIES_${unit.Index}`, LocalPlayer!.Hero!, range, {
				Position: unit.Position,
				RenderStyle: AbilitiesEnumParticles.selected_id - 1,
			}))
		}

		const screen = RendererSDK.WorldToScreen(unit.Position)
		if (screen === undefined || !unit.IsAlive)
			return

		const Size = ((AbilitiesScaleWorld.value - 5) * (RendererSDK.WindowSize.y / 1080))
		const position = new RectangleX(screen, new Vector2(Size, Size))
		if (position.IsZero())
			return

		const duration = data.Duration !== undefined
			? data.Duration(abil)
			: 0

		let sec = endTime - GameState.RawGameTime + duration
		if (!unit.IsVisible && unit.Name.includes("techies_land_mine")) {
			const findPlayer = EntityX.AllyUnits.find(x => x.Position.Distance2D(unit.Position) <= abil.AOERadius)
			if (findPlayer !== undefined) {
				RendererSDK.Text(sec.toFixed(1), position.Position.Clone().SubtractScalarForThis(10), Color.White, "Calibri", position.Width / 2)
				return
			}
			sec = 0
		}

		if (unit.IsVisible && unit.Name.includes("techies_land_mine") && sec > 0) {
			RendererSDK.Text(sec.toFixed(1), position.Position.Clone().SubtractScalarForThis(10), Color.White, "Calibri", position.Width / 2)
			return
		}

		position.Position.SubtractScalarForThis(20)
		const pcolor = (PlayerX.ColorX.get((Owner as Hero).PlayerID) ?? Color.Red).SetA(230)
		RectangleDraw.Image(PathX.DOTAAbilities(abil.Name), position, Color.White, 0)
		const outline = position.Clone().MultiplySizeFX(1.2)
		RectangleDraw.Image(PathX.Images.buff_outline, outline, pcolor)
		const unitlPos = position.Clone().MultiplySizeFX(0.5)
		unitlPos.X += unitlPos.Width * 0.8
		unitlPos.Y += unitlPos.Height * 0.6
		const outline2 = unitlPos.Clone().MultiplySizeFX(1.2)
		RectangleDraw.Arc(-90, sec, duration, true, position, Color.Black)
		RectangleDraw.Image(PathX.Heroes(Owner.Name), unitlPos, Color.White.SetA(230), 0)
		RectangleDraw.Image(PathX.Images.buff_outline, outline2, pcolor)
	})
}

export const MAP_HACK_ABILITIES_ENTITIES_CREATED = (ent: Entity) => {
	if (!(ent instanceof Unit) || !ent.IsEnemy())
		return
	const name = ent.Name
	if (name === "")
		return
	const data = AbilitiesEntitiesData.find(obj => name.includes(obj.nameIcon))
	if (data !== undefined) {
		MAP_HACK_DATA.AbilitiesSpecialUnits.set(ent.Index, [data, ent, GameState.RawGameTime])
	} else {
		MAP_HACK_DATA.RunTimeParticles.delete(ent.Index)
		MAP_HACK_DATA.AbilitiesSpecialUnits.delete(ent.Index)
		if (AbilitiesEnumParticles.selected_id === 0)
			return
		MAP_HACK_DATA.ParticleSDK.DestroyByKey(`MAP_HACK_ABILITIES_ENTITIES_${ent.Index}`)
	}
}

export const MAP_HACK_ABILITIES_ENTITIES_DESTROY = (ent: Entity) => {
	if (!AbilitiesState.value || !(ent instanceof Unit))
		return
	MAP_HACK_DATA.AbilitiesSpecialUnits.delete(ent.Index)
	MAP_HACK_DATA.RunTimeParticles.delete(ent.Index)
	if (AbilitiesEnumParticles.selected_id === 0)
		return
	MAP_HACK_DATA.ParticleSDK.DestroyByKey(`MAP_HACK_ABILITIES_ENTITIES_${ent.Index}`)
}

export const MAP_HACK_ABILITIES_ENTITIES_GAME_DESTROY = (name: string, obj: any) => {
	if (!AbilitiesState.value || name !== "entity_killed")
		return
	const find = MAP_HACK_DATA.AbilitiesSpecialUnits.get(obj.entindex_killed)
	if (find === undefined)
		return
	MAP_HACK_DATA.AbilitiesSpecialUnits.delete(obj.entindex_killed)
	MAP_HACK_DATA.RunTimeParticles.delete(obj.entindex_killed)
	if (AbilitiesEnumParticles.selected_id === 0)
		return
	MAP_HACK_DATA.ParticleSDK.DestroyByKey(`MAP_HACK_ABILITIES_ENTITIES_${obj.entindex_killed}`)
}

export const MAP_HACK_SOUND = (name: string, entity: Nullable<Entity | number>, startPos: Vector3) => {
	if (!AbilitiesState.value || !AbilitiesWorld.value || entity === undefined)
		return

	const Index = entity instanceof Entity ? entity.Index : entity
	const obj_created = soundAbilitiesData.find(x => x.soundCreate === name)
	const obj_destroy = soundAbilitiesData.find(x => x.soundDestroy === name)

	if (obj_created !== undefined) {
		const findOwner = EntityX.AllyHeroes.filter(x => x.Spells.some(z => z !== undefined && z.Name.includes(obj_created.nameIcon)))
		if (findOwner.length !== 0)
			return
		const find = MAP_HACK_DATA.AbilitiesSpecialUnits.get(Index)
		if (find === undefined) {
			MAP_HACK_DATA.AbilitiesSpecialUnits.set(Index, [AbilitiesEntitiesData[3], startPos, GameState.RawGameTime, Index])
			return
		}
		find[2] = GameState.RawGameTime
	}

	if (obj_destroy === undefined)
		return

	MAP_HACK_DATA.SoundAbilities.delete(Index)
}
