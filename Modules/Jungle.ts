import { Color, Creep, EntityManager, GameRules, Hero,  MinimapSDK,  npc_dota_hero_wisp,  RendererSDK, Team, Vector2, Vector3 } from "wrapper/Imports"
import { PathX, PlayerX } from "X-Core/Imports"
import { MAP_HACK_DATA } from "../data"
import { JungleMapIconSize, JungleMapImageMode, JungleMapImageSizeWorld, JungleMapState, WispState } from "../menu"
import { TemplarAssasinModel } from "../Models/Templar"

export const JUNGLE_MAP_HACK_TICK = () => {
	if (!JungleMapState.value)
		return
	MAP_HACK_DATA.JungleAttackCreeps = MAP_HACK_DATA.JungleAttackCreeps
		.filter(ar => GameRules!.RawGameTime - ar[2] < 1.5)
}

const WorldImage = (path: string, pos: Vector2, size: Vector2, circle: number = 0, color: Color = Color.White) =>  {
	RendererSDK.Image(path, pos, circle, size, color)
}

export const JUNGLE_MAP_HACK_DRAW = () => {
	if (!JungleMapState.value)
		return
	MAP_HACK_DATA.JungleAttackCreeps.forEach(([hero, pos]) => {
		if (hero.IsIllusion)
			return

		const position = Vector3.GetCenter(pos)

		try {
			// plainly ignore any exceptions
			MinimapSDK.DrawIcon(
				`heroicon_${hero.Name}`,
				position,
				JungleMapIconSize.value * 12,
				Color.White,
			)
		} catch { /***/ }

		const size = (JungleMapImageSizeWorld.value * (RendererSDK.WindowSize.y / 1080))
		const screen_pos = RendererSDK.WorldToScreen(position)
		if (screen_pos === undefined)
			return

		const vSize = new Vector2(size, size)
		const pos_attack = screen_pos.SubtractScalar(size / 2)
		const pcolor = PlayerX.ColorX.get(hero.PlayerID) ?? Color.White

		if (JungleMapImageMode.selected_id !== 0) {
			WorldImage(PathX.Heroes(hero.Name), pos_attack, vSize, -1)
			return
		}

		WorldImage(PathX.Heroes(hero.Name, false), pos_attack, vSize)
		WorldImage(PathX.Images.buff_outline, pos_attack.SubtractScalarForThis(2), vSize.AddScalarForThis(5), 0, pcolor)
	})
}

export const JUNGLE_MAP_HACK_GAME = (name: string, obj: any) => {
	if (!JungleMapState.value)
		return

	if (name !== "entity_hurt" && name !== "entity_killed")
		return

	const ent1 = EntityManager.EntityByIndex(obj.entindex_killed)
	const ent2 = EntityManager.EntityByIndex(obj.entindex_attacker)

	if (ent1 === undefined || ent2 === undefined
		|| !ent1.IsValid || !ent2.IsValid
		|| ent1.IsVisible || ent2.IsVisible
		|| (!(ent1 instanceof Hero) && !(ent2 instanceof Hero))
		|| (!(ent1 instanceof Creep && ent1.Team === Team.Neutral) && !(ent2 instanceof Creep && ent2.Team === Team.Neutral))
	) return

	const hero = ent1 instanceof Hero ? ent1 : ent2 as Hero
	const creep = hero === ent1 ? ent2 : ent1
	const array = MAP_HACK_DATA.JungleAttackCreeps.find(ar_ => ar_[0] === hero)

	if (hero instanceof npc_dota_hero_wisp && WispState.value)
		return

	if (array !== undefined) {
		if (Vector3.GetCenter(array[1]).Distance2D(creep.Position) > 300)
			array[1] = []

		array[1].push(creep.Position)
		array[2] = GameRules?.RawGameTime ?? 0

	} else {

		if (hero instanceof Hero && TemplarAssasinModel.MeldMap.has(hero.Index)) {
			TemplarAssasinModel.MeldMap.delete(hero.Index)
			MAP_HACK_DATA.ParticleSDK.DestroyByKey(`MAP_HACK_TEMPALR_MELD_${hero.Index}`)
		}

		MAP_HACK_DATA.JungleAttackCreeps.push([hero, [creep.Position], GameRules?.RawGameTime ?? 0])
	}
}
