import { Ability, Color, GameState, Hero, LocalPlayer, Modifier, ParticleAttachment_t, PARTICLE_RENDER, RendererSDK, Unit, Vector3 } from "wrapper/Imports";
import { PlayerX } from "X-Core/Imports";
import { MAP_HACK_DATA } from "../data";
import { AbilitiesEnumParticles, AbilitiesState } from "../menu";
import { ParticlesModMarker, ThinkerParticles } from "./Abilities/ThinkerData";

function RemoveParticles(key: string) {
	if (!MAP_HACK_DATA.ParticleSDK.AllParticles.has(key))
		return
	MAP_HACK_DATA.ParticleSDK.DestroyByKey(key)
}

function RangeCreate(key: string, abil: Nullable<Ability>, unit: Unit, color: Color) {
	MAP_HACK_DATA.ParticleSDK.DrawCircle(key, LocalPlayer!.Hero!, abil?.AOERadius ?? 100, {
		Position: unit.Position,
		Color: color,
		RenderStyle: AbilitiesEnumParticles.selected_id as PARTICLE_RENDER,
	})
}

export const ABILITIES_MODIFIER_DRAW = () => {
	if (!AbilitiesState.value)
		return
	MAP_HACK_DATA.ModifierTime.forEach((time, mod) => {
		const RemainingTime = Math.abs(GameState.RawGameTime - time)
		const pos = RendererSDK.WorldToScreen(mod.Parent!.Position)
		if (pos === undefined || RemainingTime <= 0)
			return
		RendererSDK.Text(RemainingTime.toFixed(1), pos, Color.White, "Calibri", 18)
	})
}

export const ABILITIES_MODIFIER_CREATE = (buff: Modifier) => {
	if (!AbilitiesState.value)
		return

	const caster = buff.Caster
	const parent = buff.Parent
	if (!caster?.IsEnemy() || parent === undefined)
		return
	const data_mod = ParticlesModMarker.get(buff.Name)
	if (data_mod !== undefined) {
		MAP_HACK_DATA.ParticleSDK.AddOrUpdate(`THINKER_MAP_HACK_MARKER_${buff.Name}_${parent.Index}`,
			data_mod[0],
			ParticleAttachment_t.PATTACH_OVERHEAD_FOLLOW,
			parent,
		)
	}
	const ability = buff.Ability
	const find_mod = ThinkerParticles.find(x => x.modName === buff.Name)
	if (find_mod === undefined || ability === undefined)
		return

	const pcolor = PlayerX.ColorX.get((caster as Hero).PlayerID) ?? Color.Red
	RangeCreate(`THINKER_MAP_HACK_RANGE_${buff.Name}_${parent.Index}`, ability, parent, pcolor)

	if (find_mod.particle !== undefined)
		MAP_HACK_DATA.ParticleSDK.AddOrUpdate(`THINKER_MAP_HACK_CUSTOM_${parent.Index}`,
			find_mod.particle,
			ParticleAttachment_t.PATTACH_ABSORIGIN_FOLLOW,
			parent,
			[0, parent.Position],
			[1, new Vector3(ability.AOERadius, 0, 0)],
		)

	const atime = ability.ActivationDelay
	MAP_HACK_DATA.ModifierTime.set(buff, GameState.RawGameTime + (atime === 0 ? 1.7 : atime))
}

export const ABILITIES_MODIFIER_DESTROY = (buff: Modifier) => {
	if (!AbilitiesState.value)
		return

	const parent = buff.Parent
	if (parent === undefined)
		return

	if (MAP_HACK_DATA.ModifierTime.has(buff))
		MAP_HACK_DATA.ModifierTime.delete(buff)

	RemoveParticles(`THINKER_MAP_HACK_RANGE_${buff.Name}_${parent.Index}`)
	RemoveParticles(`THINKER_MAP_HACK_MARKER_${buff.Name}_${parent.Index}`)
}
