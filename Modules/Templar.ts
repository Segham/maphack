import { Color, GameState, LocalPlayer, MinimapSDK, npc_dota_hero_rubick, RendererSDK, templar_assassin_meld, templar_assassin_psi_blades, Unit, Vector2 } from "wrapper/Imports"
import { PathX, RectangleDraw, RectangleX } from "X-Core/Imports"
import { MAP_HACK_DATA } from "../data"
import { AbilitiesEnumParticles, AbilitiesMiniMap, AbilitiesScaleMiniMap, AbilitiesScaleWorld, AbilitiesState, AbilitiesWorld } from "../menu"
import { TemplarAssasinModel } from "../Models/Templar"

export const ABILITIES_TA_DRAW = () => {
	if (!AbilitiesState.value)
		return

	TemplarAssasinModel.MeldMap.forEach(([abil, time], index) => {
		if (time < GameState.RawGameTime
			|| abil.Owner === undefined
			|| abil.Owner.IsVisible
			|| !abil.Owner.IsAlive
			|| (abil.Owner.BecameDormantTime + 3) < GameState.RawGameTime
		) {
			if (AbilitiesEnumParticles.selected_id !== 0)
				MAP_HACK_DATA.ParticleSDK.DestroyByKey(`MAP_HACK_TEMPALR_MELD_${index}`)

			TemplarAssasinModel.MeldMap.delete(index)
			return
		}

		if (AbilitiesMiniMap.value)
			MinimapSDK.DrawIcon(`heroicon_${abil.Owner}`, abil.Owner.Position, AbilitiesScaleMiniMap.value * 100)

		if (!AbilitiesWorld.value)
			return

		const screen = RendererSDK.WorldToScreen(abil.Owner.Position)
		if (screen === undefined)
			return

		const Size = (AbilitiesScaleWorld.value * (RendererSDK.WindowSize.y / 1080))
		const position = new RectangleX(screen, new Vector2(Size, Size))
		if (position.IsZero())
			return
		position.Position.SubtractScalarForThis(20)
		RectangleDraw.Image(PathX.DOTAAbilities(abil.Name), position, Color.White, 0)
		const outline = position.Clone().MultiplySizeFX(1.2)
		RectangleDraw.Image(PathX.Images.buff_outline, outline, Color.Red)
		const abilPos = position.Clone().MultiplySizeFX(0.5)
		abilPos.X += abilPos.Width * 0.8
		abilPos.Y += abilPos.Height * 0.6
		const outline2 = abilPos.Clone().MultiplySizeFX(1.2)
		RectangleDraw.Image(PathX.Heroes(abil.Owner.Name),
			abilPos,
			Color.White.SetA(230), 0,
		)
		RectangleDraw.Image(
			PathX.Images.buff_outline,
			outline2,
			Color.Red.SetA(230),
		)
	})
}

export const ABILITIES_TA_ANIM = (npc: Unit, acitivity: number) => {
	if (!npc.IsEnemy() || !AbilitiesState.value)
		return

	const needed_activity = npc.GetActivityForAbilityClass(templar_assassin_meld)
	if (needed_activity === undefined || acitivity !== ((npc instanceof npc_dota_hero_rubick ? 1 : 0)) + needed_activity)
		return

	const abil = npc.GetAbilityByClass(templar_assassin_meld)
	const abil2 = npc.GetAbilityByClass(templar_assassin_psi_blades)
	if (abil === undefined || abil.Owner === undefined)
		return

	TemplarAssasinModel.MeldMap.set(npc.Index, [abil, GameState.RawGameTime + 3])

	if (AbilitiesEnumParticles.selected_id === 0)
		return

	MAP_HACK_DATA.ParticleSDK.DrawCircle(`MAP_HACK_TEMPALR_MELD_${npc.Index}`, LocalPlayer!.Hero!,
		(npc.AttackRange !== 160 ? npc.AttackRange : npc.AttackRange + (abil2?.GetSpecialValue("bonus_attack_range") ?? 0)), {
			Position: npc.Position,
			RenderStyle: AbilitiesEnumParticles.selected_id - 1,
		},
	)
}
