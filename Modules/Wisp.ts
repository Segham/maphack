import { Ability, Color, Entity, EntityManager, GameState, Hero, MinimapSDK, Modifier, npc_dota_hero_wisp, RendererSDK, Vector2, Vector3 } from "wrapper/Imports"
import { PathX, PlayerX, RectangleDraw, RectangleX } from "X-Core/Imports"
import { WispMiniMap, WispScaleMiniMap, WispScaleWorld, WispState, WispWorld } from "../menu"
import { WispModel } from "../Models/Wisp"

function RealocateReturn(caster: Hero | Vector3, abil: Ability, DieTime: number, duration: number) {
	const screen_ = RendererSDK.WorldToScreen(!(caster instanceof Hero) ? caster : caster.Position)
	if (screen_ === undefined)
		return
	const Size_ = ((WispScaleWorld.value - 2) * (RendererSDK.WindowSize.y / 1080))
	const position_ = new RectangleX(screen_, new Vector2(Size_, Size_))
	if (position_.IsZero())
		return
	position_.Position.SubtractScalarForThis(20)
	const pcolor_ = !(caster instanceof Hero) ? Color.Red.SetA(230) : (PlayerX.ColorX.get(caster.PlayerID) ?? Color.Red).SetA(230)
	RectangleDraw.Image(PathX.DOTAAbilities(abil.Name), position_, Color.White, 0)
	const outline_ = position_.Clone().MultiplySizeFX(1.2)
	RectangleDraw.Image(PathX.Images.buff_outline, outline_, pcolor_)
	const unitlPos = position_.Clone().MultiplySizeFX(0.5)
	unitlPos.X += unitlPos.Width * 0.8
	unitlPos.Y += unitlPos.Height * 0.6
	RectangleDraw.Arc(-90, DieTime, duration, true, position_, Color.Black)
	const textSize = position_.Height / 2
	const pos = Vector2.FromVector3(RendererSDK.GetTextSize(DieTime.toFixed(1), "Calibri", textSize))
		.MultiplyScalarForThis(-1)
		.AddScalarX(position_.Width)
		.AddScalarY(position_.Height)
		.DivideScalarForThis(2)
		.AddScalarX(position_.X)
		.AddScalarY(position_.Y)
		.RoundForThis()
	RendererSDK.Text(DieTime.toFixed(1), pos, Color.White, "Calibri", textSize)
}

export const MAP_HACK_WISP_DRAW = () => {
	if (!WispState.value || WispModel.wisp === undefined)
		return

	WispModel.Thinkers.forEach(([startTime, duration, caster, abil, endPosition], id) => {
		const DieTime = startTime - GameState.RawGameTime + duration
		if (DieTime < 0 || !caster.IsAlive) {
			WispModel.Thinkers.delete(id)
			return
		}
		RealocateReturn(!caster.IsVisible ? WispModel.pos : caster, abil, DieTime, duration)
		RealocateReturn(endPosition, abil, DieTime, duration)
	})

	if (typeof WispModel.wisp !== "number" || !WispModel.guaranteed_hero) {
		const wisp_ = WispModel.wisp instanceof Entity ? WispModel.wisp : undefined
		if (wisp_ !== undefined && (!wisp_.IsEnemy() || wisp_.IsVisible || !wisp_.IsAlive))
			return
	}
	if (WispMiniMap.value) {
		const SizeMiniMap = WispScaleMiniMap.value * 100
		MinimapSDK.DrawIcon("heroicon_npc_dota_hero_wisp", WispModel.pos, SizeMiniMap, Color.White)
	}

	if (!WispWorld.value || !WispModel.pos.IsValid)
		return
	const screen = RendererSDK.WorldToScreen(WispModel.pos)
	if (screen === undefined || WispModel.Thinkers.size !== 0)
		return
	const Size = (WispScaleWorld.value * (RendererSDK.WindowSize.y / 1080))
	const position = new RectangleX(screen, new Vector2(Size, Size))
	if (position.IsZero())
		return

	position.Position.SubtractScalarForThis(20)
	const outline = position.Clone().MultiplySizeFX(1.2)
	const pcolor = (PlayerX.ColorX.get((WispModel.wisp as Hero)?.PlayerID) ?? Color.Red).SetA(230)
	RectangleDraw.Image(PathX.Images.buff_outline, outline, pcolor)
	RectangleDraw.Image(PathX.Heroes((WispModel.wisp as Hero).Name, false), position, Color.White, 0)
}

export const MAP_HACK_WISP_PARTICLE_CREATED = (id: number, handle: bigint) => {
	if (!WispState.value || handle !== 2971384879877296313n)
		return
	WispModel.par_id = id
}

export const MAP_HACK_WISP_PARTICLE_UPDATE_ENT = (id: number, ent: Nullable<Entity | number>, position: Vector3) => {
	if (!WispState.value)
		return
	if (id === WispModel.par_id || (WispModel.par_id === -1 && ((ent !== undefined && ent === WispModel.wisp) || (ent instanceof npc_dota_hero_wisp && ent.IsEnemy())))) {
		WispModel.pos = position
		WispModel.wisp = ent as Entity | number
	}
}

export const MAP_HACK_WISP_GAME = (name: string, obj: any) => {
	if (!WispState.value || name !== "dota_on_hero_finish_spawn" || obj.hero !== "npc_dota_hero_wisp")
		return
	const ent_id = obj.heroindex as number
	WispModel.wisp = EntityManager.EntityByIndex(ent_id) ?? ent_id
	WispModel.guaranteed_hero = true
}

export const MAP_HACK_WISP_MODFIRE_DESTROY = (modifire: Modifier) => {
	if (modifire.Name !== "modifier_wisp_relocate_thinker"
		|| modifire.Caster === undefined
		|| modifire.Ability === undefined
		|| !modifire.Caster.IsEnemy())
		return
	const is_abil = modifire.Ability.Name === "wisp_relocate"
	if (!is_abil || !(modifire.Caster instanceof Hero))
		return
	WispModel.Thinkers.set(modifire.Caster.Index, [
		GameState.RawGameTime,
		modifire.Ability.GetSpecialValue("return_time"),
		modifire.Caster,
		modifire.Ability,
		modifire.Caster.Position,
	])
}
