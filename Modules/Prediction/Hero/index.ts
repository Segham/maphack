import { Color, GameState, MinimapSDK, npc_dota_hero_wisp, RendererSDK, Vector2 } from "wrapper/Imports"
import { EntityX, PathX, PlayerX, PredictionX, RectangleDraw, RectangleX } from "X-Core/Imports"
import { PredictionHeroMiniMap, PredictionHeroScaleMiniMap, PredictionHeroScaleWorld, PredictionHeroState, PredictionHeroWorld, WispState } from "../../../menu"

export const PREDICTION_HEROES_DRAW = () => {
	if (!PredictionHeroState.value)
		return
	EntityX.EnemyHeroes.forEach(hero => {

		if (WispState.value && hero instanceof npc_dota_hero_wisp)
			return

		const BecameDormantTime = hero.BecameDormantTime
		if (!hero.IsMoving || BecameDormantTime <= 0 || hero.IsVisible || !hero.IsAlive)
			return

		const delay = GameState.RawGameTime - BecameDormantTime
		const Prediction = PredictionX.PseudoPrediction(hero, delay)
		if (Prediction.x >= 8000 || Prediction.y >= 8000)
			return
		const pcolor = (PlayerX.ColorX.get(hero.PlayerID) ?? Color.Red).SetA(230)
		if (PredictionHeroMiniMap.value) {
			MinimapSDK.DrawIcon(`ping_shop`, Prediction, (PredictionHeroScaleMiniMap.value * 100) * 1.3, pcolor)
			MinimapSDK.DrawIcon(`heroicon_${hero.Name}`, Prediction, (PredictionHeroScaleMiniMap.value * 100))
		}
		if (!PredictionHeroWorld.value)
			return
		const screen = RendererSDK.WorldToScreen(Prediction)
		if (screen === undefined)
			return
		const Size = PredictionHeroScaleWorld.value * (RendererSDK.WindowSize.y / 1080)
		const position = new RectangleX(screen, new Vector2(Size, Size))
		if (position.IsZero())
			return
		const outline = position.Clone().MultiplySizeFX(1.2)
		RectangleDraw.Image(PathX.Images.buff_outline, outline, pcolor)
		RectangleDraw.Image(PathX.Heroes(hero.Name, false), position, Color.White, 0)
	})
}
