import { ArrayExtensions, Creep, DOTA_GameState, Entity, GameRules, LocalPlayer, MinimapSDK, RendererSDK } from "wrapper/Imports";
import { LanePosition, MAP_HACK_DATA } from "../../../data";
import { PredictionCreepsMiniMap, PredictionCreepsMninColor, PredictionCreepsoWorld, PredictionCreepsScaleMiniMap, PredictionCreepsScaleWorld, PredictionCreepsState, PredictionCreepsWorldColor } from "../../../menu";
import { CreepWave } from "../../../Models/index";

export const CREEP_WAVE_TICK = () => {
	if (!PredictionCreepsState.value || MAP_HACK_DATA.GameSleeper.Sleeping("MAP_HACK_SPAWN_CREEPS"))
		return
	MAP_HACK_DATA.lanePaths.Init(LocalPlayer!.Hero!.Team)
	if (GameRules!.GameState === DOTA_GameState.DOTA_GAMERULES_STATE_PRE_GAME)
		return
	if ((GameRules?.GameTime ?? 0) % 30 < 1) {
		MAP_HACK_DATA.CreepWaves.filter(x => !x.IsSpawned).forEach(creepWave => creepWave.Spawn())
		MAP_HACK_DATA.GameSleeper.ResetKey("MAP_HACK_SPAWN_CREEPS")
		MAP_HACK_DATA.GameSleeper.Sleep(25000, "MAP_HACK_SPAWN_CREEPS");
	}
}

export const CREEP_WAVE_POST_UPDATE = () => {
	if (!PredictionCreepsState.value)
		return

	for (var i = MAP_HACK_DATA.CreepWaves.length - 1; i > -1; i--) {
		const wave = MAP_HACK_DATA.CreepWaves[i]
		if (!wave.IsSpawned)
			continue
		if (!wave.IsValid) {
			ArrayExtensions.arrayRemove(MAP_HACK_DATA.CreepWaves, wave)
			continue
		}
		const merge = MAP_HACK_DATA.CreepWaves.find(x => x.IsSpawned
			&& x.Lane === wave.Lane && x.index !== wave.index
			&& x.PredictedPosition.Distance2D(wave.PredictedPosition) < 500,
		)
		if (merge !== undefined) {
			wave.Creeps = [...merge.Creeps]
			ArrayExtensions.arrayRemove(MAP_HACK_DATA.CreepWaves, wave)
			continue
		}
		wave.Update()
	}
}

export const CREEP_WAVE_ENTITY_CREATE = (ent: Entity) => {
	if (!PredictionCreepsState.value || !(ent instanceof Creep) || !ent.IsEnemy())
		return
	if (!ent.IsLaneCreep || ent.IsSpawned)
		return
	const lane = MAP_HACK_DATA.lanePaths.GetCreepLane(ent)
	if (lane === LanePosition.Unknown)
		return
	let wave = MAP_HACK_DATA.CreepWaves.find(x => !x.IsSpawned && x.Lane === lane)
	if (wave === undefined) {
		MAP_HACK_DATA.CreepWaves.push(wave = new CreepWave(lane, MAP_HACK_DATA.lanePaths.GetLanePath(lane), MAP_HACK_DATA.CreepWaveIndex))
		MAP_HACK_DATA.CreepWaveIndex += 1
	}
	if (!wave.Creeps.includes(ent))
		wave.Creeps.push(ent)
}

export const CREEP_WAVE_ENTITY_DESTROY = (ent: Entity) => {
	if (!PredictionCreepsState.value || !(ent instanceof Creep))
		return
	const wave = MAP_HACK_DATA.CreepWaves.find(x => x.Creeps.includes(ent))
	if (wave === undefined)
		return
	ArrayExtensions.arrayRemove(wave.Creeps, ent)
	if (wave.IsValid)
		return
	ArrayExtensions.arrayRemove(MAP_HACK_DATA.CreepWaves, wave)
}

export const CREEP_WAVE_DRAW = () => {
	if (!PredictionCreepsState.value)
		return

	MAP_HACK_DATA.CreepWaves.forEach(wave => {
		if (!wave.IsSpawned || wave.IsVisible)
			return

		const position = wave.PredictedPosition
		const countCreeps = wave.Creeps.length.toString()

		if (PredictionCreepsMiniMap.value) {
			MinimapSDK.DrawIcon(
				"siege",
				position,
				(PredictionCreepsScaleMiniMap.value * 100),
				PredictionCreepsMninColor.selected_color,
			)
		}

		if (!PredictionCreepsoWorld.value)
			return

		const screen = RendererSDK.WorldToScreen(position.Clone())
		if (screen === undefined)
			return
		RendererSDK.Text(
			countCreeps,
			screen,
			PredictionCreepsWorldColor.selected_color,
			"Calibri",
			PredictionCreepsScaleWorld.value,
		)
	})
}
