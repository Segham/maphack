import { Color, GameState, Hero, MinimapSDK, Player, RendererSDK, Vector2 } from "wrapper/Imports"
import { EntityX, PathX, PlayerX, PredictionX, RectangleDraw, RectangleX } from "X-Core/Imports"
import { PredictionCouriersMiniMap, PredictionCouriersScaleMiniMap, PredictionCouriersScaleWorld, PredictionCouriersState, PredictionCouriersWorld } from "../../../menu"

export const PREDICTION_COURIERS_DRAW = () => {
	if (!PredictionCouriersState.value || EntityX.EnemyCourier.length <= 0)
		return

	EntityX.EnemyCourier.forEach(courier => {
		const BecameDormantTime = courier.BecameDormantTime
		if (!courier.IsMoving || BecameDormantTime <= 0 || !courier.IsAlive || courier.IsVisible)
			return
		const delay = GameState.RawGameTime - BecameDormantTime
		const Prediction = PredictionX.PseudoPrediction(courier, delay)
		if (Prediction.x >= 8000 || Prediction.y >= 8000)
			return

		if (PredictionCouriersMiniMap.value)
			MinimapSDK.DrawIcon(`courier${courier.IsFlying ? "_flying" : ""}`, Prediction, (PredictionCouriersScaleMiniMap.value * 100))

		if (!PredictionCouriersWorld.value)
			return

		const screen = RendererSDK.WorldToScreen(Prediction)
		if (screen === undefined)
			return
		const pcolor = (PlayerX.ColorX.get((courier.Owner as Hero)?.PlayerID) ?? Color.Red).SetA(230)
		const Size = PredictionCouriersScaleWorld.value * (RendererSDK.WindowSize.y / 1080)
		const position = new RectangleX(screen, new Vector2(Size, Size))
		if (position.IsZero())
			return
		const outline = position.Clone().MultiplySizeFX(1.2)
		RectangleDraw.Image(PathX.Images.buff_outline, outline, pcolor)
		RectangleDraw.Image("panorama/images/hud/reborn/icon_courier_standard_psd.vtex_c", position, Color.White, 0)
		if (!(courier.Owner instanceof Player) || courier.Owner.Hero === undefined)
			return
		const pos2 = position.Clone().MultiplySizeFX(0.5)
		pos2.X += pos2.Width * 0.8
		pos2.Y += pos2.Height * 0.6
		const outline2 = pos2.Clone().MultiplySizeFX(1.5)
		RectangleDraw.Image(PathX.Heroes(courier.Owner.Hero.Name, false), pos2, Color.White.SetA(230), 0)
		RectangleDraw.Image(PathX.Images.buff_outline, outline2, pcolor)
	})
}
