import { Color, Entity, EntityManager, GameState, item_ward_dispenser, item_ward_observer, item_ward_sentry, LocalPlayer, MinimapSDK, RendererSDK, Vector2, WardObserver, WardTrueSight } from "gitlab.com/FNT_Rework/wrapper/wrapper/Imports"
import { EntityX, PathX, RectangleDraw, RectangleX } from "X-Core/Imports"
import { MAP_HACK_DATA } from "../../../data"
import { EventMapHack } from "../../../Events/Custom/Interface"
import { EmiterTimeWard } from "../../../Events/Custom/On"
import { PredictionHeroScaleWorld, PredictionWardsCooldown, PredictionWardsMiniMap, PredictionWardsParicleColorObserver, PredictionWardsParicleColorSentry, PredictionWardsState, PredictionWardsTypeRender, PredictionWardsWorld } from "../../../menu"
import { WardModel } from "../../../Models/Wards"

export const MAP_HACK_WARD_TRACKER_TICK = () => {

	if (!PredictionWardsState.value || EntityX.EnemyHeroes.length === 0)
		return

	EmiterTimeWard()

	if (GameState.RawGameTime - MAP_HACK_DATA.WardCaptureTiming < 0.1)
		return

	EntityX.EnemyHeroes.forEach(hero => {

		if (!hero.IsAlive || !hero.IsVisible) {
			MAP_HACK_DATA.WardDispenserCount.delete(hero.Index)
			MAP_HACK_DATA.WardCaptureTiming = GameState.RawGameTime
			return
		}

		const sentry = hero.GetItemByClass(item_ward_sentry, true)
		const observer = hero.GetItemByClass(item_ward_observer, true)
		const dispenser = hero.GetItemByClass(item_ward_dispenser, true)

		let sentry_stack = 0
		let observer_stack = 0

		const owner_idx = hero.Index
		const unique_id = owner_idx + Math.floor(GameState.RawGameTime)

		if (sentry !== undefined) {
			sentry_stack = sentry.CurrentCharges
		} else if (observer !== undefined) {
			observer_stack = observer.CurrentCharges
		} else if (dispenser !== undefined) {
			sentry_stack = dispenser.SecondaryCharges
			observer_stack = dispenser.CurrentCharges
		}

		const wardDispenserCount_ = MAP_HACK_DATA.WardDispenserCount.get(owner_idx)

		if (wardDispenserCount_ === undefined) {
			if (sentry_stack > 0 || observer_stack > 0) {
				MAP_HACK_DATA.WardCaptureTiming = GameState.RawGameTime
				MAP_HACK_DATA.WardDispenserCount.set(owner_idx, [sentry_stack, observer_stack])
			}
			return
		}

		MAP_HACK_DATA.WardDispenserCount.set(owner_idx, [sentry_stack, observer_stack])
		MAP_HACK_DATA.WardCaptureTiming = GameState.RawGameTime

		if (!MAP_HACK_DATA.TickWardPlace)
			return

		if (!(wardDispenserCount_[0] < sentry_stack || wardDispenserCount_[1] < observer_stack)) {

			let ward_type
			let time = 360
			let baseCastRange = hero.CastRangeBonus
			if (wardDispenserCount_[0] > sentry_stack && (!WardModel.IsDropped(hero))) {
				time = 480
				ward_type = "sentry"
			} else if (wardDispenserCount_[1] > observer_stack && (!WardModel.IsDropped(hero))) {
				ward_type = "observer"
			}

			if (ward_type === undefined)
				return

			if ((baseCastRange / 2) <= 250)
				baseCastRange = 400

			const obj = {
				hero,
				type: ward_type,
				entity: undefined,
				position: hero.InFront(baseCastRange / 2),
				time: Math.floor(GameState.RawGameTime + time),
				particleID: unique_id,
			}

			MAP_HACK_DATA.WardsData[unique_id] = obj
			WardModel.PlaceWard(hero, ward_type, unique_id, (baseCastRange / 2))
			MAP_HACK_DATA.TickWardPlace = false
		}
	})
}

export const MAP_HACK_WARD_TRACKER_DRAW = () => {

	if (!PredictionWardsState.value || MAP_HACK_DATA.WardsData.length <= 0)
		return

	MAP_HACK_DATA.WardsData.forEach(data => {

		if (data.time < GameState.RawGameTime)
			return

		const type = data.type
		if (PredictionWardsMiniMap.value) {
			MinimapSDK.DrawIcon("ward_obs", data.position, 600,
				(type === "sentry"
					? PredictionWardsParicleColorSentry.selected_color
					: PredictionWardsParicleColorObserver.selected_color
				),
			)
		}

		if (!PredictionWardsWorld.value)
			return

		const screen_pos = RendererSDK.WorldToScreen(data.position)
		if (screen_pos === undefined)
			return

		const sec = Math.floor(data.time - GameState.RawGameTime)
		const timer = Math.floor(sec / 60) + ":" + ((sec % 60) < 10 ? "0" : "") + sec % 60

		if (PredictionWardsTypeRender.selected_id === 0) {

			RendererSDK.Image(PathX.Images.icon_ward, screen_pos.SubtractScalarForThis(15), -1, new Vector2(30, 30),
				(type === "sentry" ? new Color(15, 0, 221) : new Color(222, 170, 0)),
			)

			if (PredictionWardsCooldown.value)
				RendererSDK.Text(timer, screen_pos.AddScalarY(50), Color.White, "Calibri", 20)
		}

		if (PredictionWardsTypeRender.selected_id !== 2)
			return

		const Size = ((PredictionHeroScaleWorld.value - 5) * (RendererSDK.WindowSize.y / 1080))
		const position = new RectangleX(screen_pos, new Vector2(Size, Size))
		if (position.IsZero())
			return

		position.Position.SubtractScalarForThis(20)

		if ((!data.entity?.IsVisible ?? false) || LocalPlayer!.Hero!.Distance2D(data.position) >= LocalPlayer!.Hero!.AttackRange) {
			const Image = type === "sentry" ? "item_ward_sentry" : "item_ward_observer"
			RectangleDraw.Image(PathX.DOTAItems(Image), position, Color.White, 0)
			if (!PredictionWardsCooldown.value) {
				RectangleDraw.Image(PathX.Images.softedge_circle_sharp, position, Color.Black.SetA(160))
				RectangleDraw.Image(PathX.Images.buff_outline, position, Color.Red)
				return
			}
			RectangleDraw.Arc(-90, sec, (type === "sentry" ? 480 : 360), true, position, Color.Black, Color.Black)
		}

		const textSize = position.Height / 2.5
		const pos = Vector2.FromVector3(RendererSDK.GetTextSize(timer, "Calibri", textSize))
			.MultiplyScalarForThis(-1)
			.AddScalarX(position.Width)
			.AddScalarY(position.Height)
			.DivideScalarForThis(2)
			.AddScalarX(position.X)
			.AddScalarY(position.Y)
			.RoundForThis()
		RendererSDK.Text(timer, pos, Color.White, "Calibri", textSize)
	})
}

export const MAP_HACK_WARD_TRACKER_GAME_EVENT = (name: string, obj: any) => {
	if (name !== "entity_killed" || !PredictionWardsState.value)
		return
	const ent = EntityManager.EntityByIndex(obj.entindex_killed)
	if (ent === undefined || !(ent instanceof WardObserver || ent instanceof WardTrueSight) || !ent.IsEnemy())
		return
	WardModel.EmitRemoveWard(ent)
}

export const MAP_HACK_WARD_TRACKER_ENTITY_CREATED = (ent: Entity) => {
	if (!PredictionWardsState.value || !(ent instanceof WardObserver) || !ent.IsEnemy())
		return
	EventMapHack.emit("SetWard", false, ent)
}

export const MAP_HACK_WARD_TRACKER_PARTICLE_CREATED = (path: string, ent: Nullable<Entity | number>) => {
	if (!PredictionWardsState.value)
		return

	if (path === "particles/items2_fx/ward_spawn_generic.vpcf") {
		MAP_HACK_DATA.TickWardPlace = true
		return
	}

	if (path !== "particles/generic_gameplay/damage_flash.vpcf"
		|| !(ent instanceof WardObserver || ent instanceof WardTrueSight) || !ent.IsEnemy() || ent.IsAlive)
		return

	WardModel.EmitRemoveWard(ent)
}
