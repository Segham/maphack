import { EntityX, PathX, PlayerX, RectangleDraw, RectangleX } from "gitlab.com/FNT_Rework/x-core/Imports"
import { Color, Entity, EntityManager, Hero, MinimapSDK, RendererSDK, Unit, Vector2, Vector3 } from "wrapper/Imports"
import { MAP_HACK_DATA } from "../data"
import {  AbilitiesMiniMap, AbilitiesScaleMiniMap, AbilitiesState, PredictionHeroScaleWorld } from "../menu"
import { TechiesModel } from "../Models/index"

export const MAP_HACK_TECHIES_TICK = () => {
	MAP_HACK_DATA.AllTechiesMines.forEach(([allMines, pos, name, abil], index) => {
		const IsVisible = EntityX.EnemyRemoteMine.some(x => x.IsVisible && pos.Distance2D(x.Position) <= abil.AOERadius + 150)
		MAP_HACK_DATA.AllTechiesMinesMap.set(index, [allMines.length, pos, name, abil, IsVisible])
	})
}

export const MAP_HACK_TECHIES_DRAW = () => {
	if (!AbilitiesState.value)
		return

	MAP_HACK_DATA.AllTechiesMinesMap.forEach(([allMines, pos, name, abil, isVisible]) => {

		if (AbilitiesMiniMap.value)
			MinimapSDK.DrawIcon("ping_crosshairs3", pos, AbilitiesScaleMiniMap.value * 50, Color.Red)

		const screen = RendererSDK.WorldToScreen(pos)
		if (screen === undefined || name === undefined || isVisible)
			return

		const Size = (PredictionHeroScaleWorld.value * (RendererSDK.WindowSize.y / 1080))
		const position = new RectangleX(screen, new Vector2(Size, Size))
		if (position.IsZero())
			return
		position.Position.SubtractScalarForThis(20)
		const owner = abil.Owner
		if (owner === undefined)
			return
		const pcolor = (PlayerX.ColorX.get((owner as Hero).PlayerID) ?? Color.Red).SetA(230)
		RectangleDraw.Image(PathX.DOTAAbilities(abil.Name), position, Color.White, 0)
		const outline = position.Clone().MultiplySizeFX(1.2)
		RectangleDraw.Image(PathX.Images.buff_outline, outline, pcolor)
		const unitlPos = position.Clone().MultiplySizeFX(0.5)
		unitlPos.X += unitlPos.Width * 0.8
		unitlPos.Y += unitlPos.Height * 0.6
		const outline2 = unitlPos.Clone().MultiplySizeFX(1.2)
		RectangleDraw.Image(PathX.Images.buff_outline, outline2, Color.Red.SetA(230))
		RectangleDraw.Image(PathX.Heroes(owner.Name), unitlPos, Color.White.SetA(230), 0)
		const textSize = position.Height / 2
		const text_position = Vector2.FromVector3(RendererSDK.GetTextSize("x" + allMines, "Calibri", textSize))
			.MultiplyScalarForThis(-1)
			.AddScalarX(position.Width)
			.AddScalarY(position.Height)
			.DivideScalarForThis(2)
			.AddScalarX(position.X)
			.AddScalarY(position.Y)
			.RoundForThis()
		RendererSDK.Text("x" + allMines, text_position, Color.White, "Calibri", outline2.Width)
	})
}

export const MAP_HACK_TECHIES_ENTITY_DESTROY = (ent: Entity) => {
	if (!(ent instanceof Unit))
		return
	TechiesModel.EntityDestroyed(ent)
}

export const MAP_HACK_TECHIES_GAME = (name: string, obj: any) => {
	if (!AbilitiesState.value || name !== "entity_killed")
		return
	const entity = EntityManager.EntityByIndex(obj.entindex_killed)
	if (!(entity instanceof Unit))
		return
	TechiesModel.EntityDestroyed(entity)
}

export const MAP_HACK_TECHIES_PARTICLES_CREATED = (id: number, path: string, ent: Nullable<Entity | number>) => {
	if (!AbilitiesState.value)
		return

	if (path.includes("techies_remote_mine_plant") && ent !== undefined && typeof ent !== "number" && ent.IsEnemy()) {
		const replace = path.replace(path, "techies_remote_mine")
		MAP_HACK_DATA.WaitingSpawn.set(id, [replace, ent])
	}
	if (path.includes("techies_stasis_trap_plant")) {

		const replace = path.replace(path, "techies_stasis_trap")
		MAP_HACK_DATA.WaitingSpawn.set(id, [replace])
	}

	if (path.includes("techies_stasis_trap_explode")) {
		const replace = path.replace(path, "techies_stasis_trap")
		MAP_HACK_DATA.WaitingExplode.set(id, replace)
	}

	if (path.includes("techies_remote_mines_detonate")) {
		const replace = path.replace(path, "techies_remote_mine")
		MAP_HACK_DATA.WaitingExplode.set(id, replace)
	}
}

export const MAP_HACK_TECHIES_PARTICLES_UPDATE_ENT = (id: number, controlPoint: number, ent: Entity | number, position: Vector3) => {
	if (!AbilitiesState.value)
		return
	const IDSpawn = MAP_HACK_DATA.WaitingSpawn.get(id)
	const IDSpawnExplode = MAP_HACK_DATA.WaitingExplode.get(id)
	if (controlPoint === 2 && IDSpawn !== undefined)
		TechiesModel.CreateMine(id, position, IDSpawn[0], IDSpawn[1] as Unit)
	if (controlPoint !== 0 || IDSpawnExplode === undefined)
		return
	TechiesModel.DeleteMineByUpdate(IDSpawnExplode, position)
}

export const MAP_HACK_TECHIES_PARTICLES_UPDATE = (id: number, controlPoint: number, position: Vector3) => {
	if (!AbilitiesState.value)
		return
	const IDSpawnExplode = MAP_HACK_DATA.WaitingExplode.get(id)
	if (controlPoint !== 0 || IDSpawnExplode === undefined)
		return
	TechiesModel.DeleteMineByUpdate(IDSpawnExplode, position)
}

export const MAP_HACK_TECHIES_PARTICLES_DESTROY = (id: number) => {
	if (!AbilitiesState.value)
		return
	MAP_HACK_DATA.WaitingSpawn.delete(id)
	TechiesModel.DeleteMineByCriteria(([, particle_id]) => id === particle_id)
}
