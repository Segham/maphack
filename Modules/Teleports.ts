import { MAP_HACK_DATA } from "../data";
import { TeleportsMiniMap, TeleportsState, TeleportsWorld, TeleportsWorldParticles } from "../menu"
import { Teleports } from "../Models/index";
import { ArrayExtensions, Color, EntityManager, Hero, item_travel_boots, item_travel_boots_2, Outpost, Sleeper, Vector3 } from "../wrapper/Imports";
import { EntityX } from "../X-Core/Imports";

export const TELEPORTS_DESTROY = (id: number) => {
	if (MAP_HACK_DATA.TeleportsMap.has(id))
		MAP_HACK_DATA.TeleportsMap.delete(id)
	if (TeleportsWorldParticles.value)
		MAP_HACK_DATA.ParticleSDK.DestroyByKey(`TP_${id}`)
}
function GetDuration(hero: Hero, endPos: Vector3, start: boolean) {

	let duration = 3

	const find_Outpost = EntityX.EnemyBuilding.find(ent => ent instanceof Outpost)

	if (find_Outpost !== undefined && find_Outpost.Distance2D(endPos) < 1000)
		duration = 6

	if (start || hero.GetItemByClass(item_travel_boots) !== undefined || hero.GetItemByClass(item_travel_boots_2) !== undefined)
		return MAP_HACK_DATA.TELEPORTS.find(x => x.hero.Index === hero.Index)?.RemainingDuration ?? duration

	const find_fountain = EntityX.EnemyBuilding.find(ent => ent.ClassName === "CDOTA_Unit_Fountain" || ent.ClassName === "CDOTA_BaseNPC_NeutralItemStash")

	if (find_fountain !== undefined && find_fountain.Distance2D(endPos) < 1000)
		return duration

	const sleepers = [...MAP_HACK_DATA.TeleportSleepers.entries()]
		.filter(([x, z]) => z.Sleeping("SLEEP_TELEPORTS") && x.Distance2D(endPos) < MAP_HACK_DATA.TeleportCheckRadius).length

	if (sleepers > 0)
		duration += (sleepers * 0.5) + 1.5

	MAP_HACK_DATA.TeleportSleepers.set(endPos, MAP_HACK_DATA.GameSleeper);
	MAP_HACK_DATA.GameSleeper.Sleep(MAP_HACK_DATA.TeleportCheckDuration, "SLEEP_TELEPORTS")
	return duration
}

function RemoveSleepers(sleeper: Sleeper, vec: Vector3) {
	if (sleeper.Sleeping("SLEEP_TELEPORTS"))
		return
	MAP_HACK_DATA.TeleportSleepers.delete(vec)
}

// TODO monitor on creeps
export const TELEPORTS_CREATE = (id: number, name: string) => {
	if (!TeleportsState.value)
		return

	if (!name.includes("teleport_start") && !name.includes("teleport_end"))
		return

	if (name.includes("teleport_start"))
		MAP_HACK_DATA.TeleportsMap.set(id, [name, true])
	if (name.includes("teleport_end"))
		MAP_HACK_DATA.TeleportsMap.set(id, [name, false])
}

export const TELEPORTS_UPDATED = (id: number, controlPoint: number, pos: Vector3) => {
	if (!TeleportsState.value)
		return

	const data = MAP_HACK_DATA.TeleportsMap.get(id)
	if (data === undefined || pos.IsZero())
		return

	const color = new Color(0, 0, 0, 0)

	if (controlPoint === 0)
		data[3] = pos

	// TODO
	if (controlPoint === 2) color.SetColor(
		Number((255 * pos.x).toFixed(2)),
		Number((255 * pos.y).toFixed(2)),
		Number((255 * pos.z).toFixed(2)),
	)

	const pID = [...MAP_HACK_DATA.PlColor.entries()].find(([x]) => x.Equals(color))
	const hero = EntityManager.GetEntitiesByClass(Hero).find(x => x.IsEnemy() && x.PlayerID === (pID !== undefined && pID[1]))
	if (hero === undefined || (hero.IsVisible && data[1]))
		return
	data[4] = hero
	data[5] = color
}

export const TELEPORTS_UPDATED_ENT = (id: number, position: Vector3) => {
	if (!TeleportsState.value)
		return
	const data = MAP_HACK_DATA.TeleportsMap.get(id)
	if (data === undefined || position.IsZero())
		return

	data[2] = position

	if (!(data[4] instanceof Hero) || data[3] === undefined || data[5] === undefined)
		return
	const duration = GetDuration(data[4], data[3], data[1])

	MAP_HACK_DATA.TELEPORTS.push(
		new Teleports(data[4], id, position, data[3], duration, data[5]),
	)
}

export const TELEPORTS_DRAW = () => {
	if (!TeleportsState.value)
		return

	for (let i = MAP_HACK_DATA.TELEPORTS.length - 1; i > -1; i--) {
		const teleport = MAP_HACK_DATA.TELEPORTS[i]
		if (!teleport.IsValid) {
			ArrayExtensions.arrayRemove(MAP_HACK_DATA.TELEPORTS, teleport)
			continue
		}

		if (TeleportsWorld.value)
			teleport.DrawMap()

		if (TeleportsMiniMap.value)
			teleport.DrawMiniMap()
	}

	MAP_HACK_DATA.TeleportSleepers.forEach(RemoveSleepers)
}
