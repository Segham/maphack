import { Creep, GameState, Vector3 } from "wrapper/Imports";
import { LanePosition } from "../data";

export class CreepWave {
	public index = 0
	public SpawnTime = 0
	public IsSpawned = false
	public LastVisibleTime = 0
	public Creeps: Creep[] = []
	public Path: Vector3[] = []
	public Lane = LanePosition.Unknown

	private lastPoint = 0
	private pathLength = 0
	private endPosition = new Vector3()
	private predictedPosition = new Vector3()
	private remainingPath: Nullable<Vector3[]>
	private lastVisiblePosition = new Vector3()

	constructor(lane: LanePosition, path: Vector3[], index: number) {
		this.Lane = lane;
		this.Path = path;
		this.index = index
		this.pathLength = this.Path.length;
		this.endPosition = path[this.pathLength - 1];
		this.predictedPosition = path[0]
	}

	public get IsValid() {
		return this.Creeps.some(x => x.IsValid && x.IsAlive) && this.PredictedPosition.Distance2D(this.endPosition) > 300
	}

	public get IsVisible() {
		return this.Creeps.some(x => x.IsValid && x.IsVisible)
	}

	public get Position() {
		const creeps = this.Creeps.filter(x => x.IsValid && x.IsVisible)
		this.lastVisiblePosition = creeps.reduce((position, creep) => position.Add(creep.Position), new Vector3()).DivideScalar(creeps.length)
		this.LastVisibleTime = GameState.RawGameTime
		this.remainingPath = undefined
		return this.lastVisiblePosition
	}

	public get PredictedPosition() {
		return this.predictedPosition
	}

	public set PredictedPosition(value) {

		this.predictedPosition = value

		if (this.predictedPosition.Distance2D(this.Path[this.lastPoint]) < 500)
			this.lastPoint = Math.min(this.lastPoint + 1, this.pathLength - 1)
	}

	public get RemainingPath(): Vector3[] {
		if (this.remainingPath !== undefined)
			return this.remainingPath

		this.remainingPath = []
		const remainingPoints = this.pathLength - this.lastPoint
		this.remainingPath[0] = this.lastVisiblePosition
		this.Copy(this.Path, this.lastPoint, this.remainingPath, 1, remainingPoints)
		return this.remainingPath
	}

	public get WasVisible() {
		return this.Creeps.some(x => x.IsSpawned)
	}

	public Spawn() {
		this.IsSpawned = true
		this.SpawnTime = GameState.RawGameTime + 0.4
	}

	public Update() {
		if (!this.WasVisible) {
			this.PredictedPosition = this.PositionAfter(this.Path, GameState.RawGameTime - this.SpawnTime, 325)
		} else if (this.IsVisible) {
			this.PredictedPosition = this.Position
		} else {
			if (this.LastVisibleTime <= 0)
				this.LastVisibleTime = GameState.RawGameTime
			this.PredictedPosition = this.PositionAfter(this.RemainingPath, GameState.RawGameTime - this.LastVisibleTime, 325)
		}
	}

	private Copy(sourceArray: Vector3[], sourceIndex: number, destinationArray: Vector3[], destinationIndex: number, length: number) {
		while (length--) destinationArray[destinationIndex++] = sourceArray[sourceIndex++];
	}

	private PositionAfter(path: Vector3[], time: number, speed: number) {
		let distance = Math.max(0, time) * speed
		for (let i = 0; i <= path.length - 2; i++) {
			const from = path[i]
			const to = path[i + 1]
			const d = to.Distance(from)
			if (d > distance)
				return from.Extend(to, distance)
			distance -= d
		}
		return path[path.length - 1]
	}
}
