import { Ability, Entity, Hero, Vector3 } from "wrapper/Imports"

export class WispModel {

	public static wisp: Entity | number | undefined
	public static pos = new Vector3().Invalidate()
	public static par_id = -1
	public static guaranteed_hero = false
	public static Thinkers = new Map<number, [number, number, Hero, Ability, Vector3]>()

	public static Dispose() {
		this.par_id = -1
		this.wisp = undefined
		this.pos.Invalidate()
		this.Thinkers.clear()
		this.guaranteed_hero = false
	}
}
