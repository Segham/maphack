import { ArrayExtensions, Entity, EntityManager, GameState, Hero, item_ward_dispenser, item_ward_observer, item_ward_sentry, LocalPlayer, ParticleAttachment_t, PhysicalItem, Vector3, WardObserver, WardTrueSight } from "wrapper/Imports";
import { IDATAWards, MAP_HACK_DATA } from "../data";
import { EventMapHack } from "../Events/Custom/Interface";
import { PredictionWardsParicleColorObserver, PredictionWardsParicleColorSentry, PredictionWardsParicleState, PredictionWardsParicleStyle, PredictionWardsParicleWidth, PredictionWardsPingAllies, PredictionWardsTypeRender, PredictionWardsWorld } from "../menu";

export class WardModel {

	public static EmitRemoveWard(ent: Entity) {
		const is_observer = ent instanceof WardObserver && ent.Name === "npc_dota_observer_wards"
		const is_sentry = ent instanceof WardTrueSight && ent.Name === "npc_dota_sentry_wards"
		if (!is_observer && !is_sentry)
			return

		const nearest_ward = ArrayExtensions.orderBy(MAP_HACK_DATA.WardsData.filter(ward =>
			(is_observer && ward.type === "observer") || (is_sentry && ward.type === "sentry")),
			ward => ent.Distance(ward.position))[0]

		if (nearest_ward === undefined || ent.Distance(nearest_ward.position) > 300)
			return

		EventMapHack.emit("RemoveWard", false, nearest_ward)
	}

	public static VisionWardCreated(ent: WardObserver | WardTrueSight, is_observer: boolean) {
		if (!ent.IsAlive || !ent.IsVisible)
			return
		const hero = ent
		const owner_idx = ent.Index
		const dieTime = ent.GetBuffByName("modifier_item_buff_ward")?.DieTime
		const unique_id = owner_idx + Math.floor(GameState.RawGameTime)
		const obj = {
			hero,
			entity: ent,
			position: ent.Position,
			type: (is_observer ? "observer" : "sentry"),
			time: dieTime ?? Math.floor(GameState.RawGameTime + (is_observer ? 360 : 480)),
			particleID: unique_id,
		}
		MAP_HACK_DATA.TickWardPlace = true
		MAP_HACK_DATA.WardsData[unique_id] = obj
		this.PlaceWard(ent, (is_observer ? "observer" : "sentry"), unique_id, 400, false)
		MAP_HACK_DATA.TickWardPlace = false
	}

	public static FixPositionWard(ent: WardObserver | WardTrueSight) {
		const is_observer = ent instanceof WardObserver && ent.Name === "npc_dota_observer_wards"
		const is_sentry = ent instanceof WardTrueSight && ent.Name === "npc_dota_sentry_wards"
		if (!is_observer && !is_sentry)
			return

		const nearest_ward = ArrayExtensions.orderBy(MAP_HACK_DATA.WardsData.filter(ward =>
			(is_observer && ward.type === "observer") || (is_sentry && ward.type === "sentry")),
			ward => ent.Distance(ward.position))[0]

		if (nearest_ward === undefined || ent.Distance2D(nearest_ward.position) > 600) {
			this.VisionWardCreated(ent, is_observer)
			return
		}

		const time = ent.GetBuffByName("modifier_item_buff_ward")?.DieTime ?? nearest_ward.time
		nearest_ward.time = time
		nearest_ward.position = ent.Position
		MAP_HACK_DATA.ParticleSDK.SetConstrolPointsByKey(nearest_ward.particleID + "map_ping", [0, ent.Position])
		MAP_HACK_DATA.ParticleSDK.SetConstrolPointsByKey(nearest_ward.particleID + "map_model", [2, ent.Position])
		MAP_HACK_DATA.ParticleSDK.SetConstrolPointsByKey(nearest_ward.particleID + nearest_ward.type, [0, ent.Position])
	}

	public static RemoveParticle(Ward: IDATAWards, str_unique: string) {
		MAP_HACK_DATA.ParticleSDK.DestroyByKey(Ward.particleID + str_unique)
	}

	public static PingAllies(hero: WardObserver | WardTrueSight | Hero, range: number, infront: boolean = true) {
		if (infront) {
			hero.InFront(range).toIOBuffer()
		} else
			hero.Position.toIOBuffer()

		// MinimapSDK.SendPing(PingType_t.ENEMY_VISION, false, hero.Index)
	}

	public static IsDropped(hero: Hero) {
		return EntityManager.GetEntitiesByClass(PhysicalItem).some(x => x !== undefined
			&& (x.Item instanceof item_ward_observer || x.Item instanceof item_ward_sentry
				|| x.Item instanceof item_ward_dispenser)
			&& x.Distance(hero.Position) < 300)
	}

	public static PlaceWard(hero: WardObserver | WardTrueSight | Hero, ward_type: string, unique_id: number, range: number, infront: boolean = true) {

		if (PredictionWardsPingAllies.value)
			this.PingAllies(hero, range, infront)

		if (!PredictionWardsWorld.value)
			return

		this.ParticleDrawPing(hero, unique_id, range, infront)
		this.ParticleDrawRadius(hero, unique_id, ward_type, range, infront)
	}

	public static ParticleDrawPing(unit: WardObserver | WardTrueSight | Hero, unique_id: number, range: number, infront: boolean = true) {
		MAP_HACK_DATA.ParticleSDK.AddOrUpdate(
			unique_id + "map_ping",
			"particles/ui_mouseactions/ping_enemyward.vpcf",
			ParticleAttachment_t.PATTACH_WORLDORIGIN,
			LocalPlayer!.Hero,
			[0, (infront ? unit.InFront(range) : unit.Position)],
			[1, new Vector3(1, 1, 1)],
			[5, new Vector3(10, 0, 0)],
		)
	}

	public static ParticleDrawRadius(unit: WardObserver | WardTrueSight | Hero, unique_id: number, ward_type: string, range: number, infront: boolean = true) {
		if (PredictionWardsTypeRender.selected_id === 1) {
			MAP_HACK_DATA.ParticleSDK.AddOrUpdate(unique_id + "map_model",
				"particles/ui_mouseactions/range_finder_generic_ward_model.vpcf",
				ParticleAttachment_t.PATTACH_ABSORIGIN,
				LocalPlayer!.Hero!,
				[2, (infront ? unit.InFront(range) : unit.Position)],
				[6, new Vector3(255, 255, 255)],
				[11, new Vector3(ward_type === "sentry" ? 1 : 0, 0, 0)],
			)
		}

		if (!PredictionWardsParicleState.value)
			return

		MAP_HACK_DATA.ParticleSDK.DrawCircle(
			unique_id + ward_type,
			LocalPlayer!.Hero!,
			(ward_type === "observer" ? 1600 : 1000), {
			Position: (infront ? unit.InFront(range) : unit.Position),
			RenderStyle: PredictionWardsParicleStyle.selected_id,
			Width: PredictionWardsParicleWidth.value,
			Color: (ward_type === "sentry"
				? PredictionWardsParicleColorSentry.selected_color
				: PredictionWardsParicleColorObserver.selected_color
			),
		})
	}

	public static Dispose() {
		MAP_HACK_DATA.WardsData = []
		MAP_HACK_DATA.WardDispenserCount.clear()
		MAP_HACK_DATA.ParticleSDK.DestroyAll(true)
	}
}
