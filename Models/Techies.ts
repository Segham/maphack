import { EntityManager, GameState, LocalPlayer, techies_remote_mines, techies_stasis_trap, Unit, Vector3 } from "wrapper/Imports"
import { MAP_HACK_DATA } from "../data"
import { AbilitiesEnumParticles } from "../menu"

export class TechiesModel {
	public static EntityDestroyed(ent: Unit) {
		const mine_name_ = /^npc_dota_(techies_remote_mine|techies_stasis_trap)$/.exec(ent.Name)
		if (mine_name_ === null)
			return
		const mine_name = mine_name_[1]
		MAP_HACK_DATA.AllTechiesMines.some((obj, j) => {
			if (obj[2] !== mine_name)
				return false
			const mines = obj[0]
			return mines.some(([vec], k) => {
				if (vec.Distance(ent.Position) > 10)
					return false
				if (mines.length !== 1) {
					mines.splice(k, 1)
					obj[1] = Vector3.GetCenter(mines.map(([vec_]) => vec_))
				} else {
					MAP_HACK_DATA.AllTechiesMines.splice(j, 1)
					MAP_HACK_DATA.AllTechiesMinesMap.delete(j)
				}

				if (AbilitiesEnumParticles.selected_id !== 0)
					MAP_HACK_DATA.ParticleSDK.DestroyByKey(`MAP_HACK_TECHIES_${mine_name}_${vec.Length}`)

				return true
			})
		})
	}

	public static CreateMine(id: number, position: Vector3, IDSpawnName: string, IDSpawnUnit: Nullable<Unit>) {
		const is_statsis = IDSpawnName === "techies_stasis_trap"
		const mine = (is_statsis ? techies_stasis_trap : techies_remote_mines)
		const Owner = IDSpawnUnit?.GetAbilityByName(IDSpawnName) ?? EntityManager.GetEntitiesByClass(mine)[0] as techies_stasis_trap | techies_remote_mines
		if (is_statsis && Owner.OwnerEntity?.IsVisible)
			return
		if (!MAP_HACK_DATA.AllTechiesMines.some(arr => this.UpdateWaitSpawn(arr, id, position, IDSpawnName))) {
			MAP_HACK_DATA.AllTechiesMines.push([[[position, id, GameState.RawGameTime, IDSpawnName]], position, IDSpawnName, Owner])
			const radius = is_statsis ? 400 : 425
			if (AbilitiesEnumParticles.selected_id === 0)
				return
			const mode_particles = AbilitiesEnumParticles.selected_id - 1
			MAP_HACK_DATA.ParticleSDK.DrawCircle(`MAP_HACK_TECHIES_${IDSpawnName}_${position.Length}`, LocalPlayer!.Hero!, radius, {
				Position: position,
				RenderStyle: mode_particles,
			})
		}
		MAP_HACK_DATA.WaitingSpawn.delete(id)
	}

	public static UpdateWaitSpawn(
		arr: [[Vector3, number, number, string][], Vector3, string, techies_stasis_trap | techies_remote_mines],
		id: number,
		position: Vector3,
		mine_name: string,
	) {
		if (arr[2] !== mine_name)
			return false
		const center = arr[1], mines = arr[0]
		if (center.Distance(position) > 100)
			return false

		if (AbilitiesEnumParticles.selected_id !== 0) {
			const lastpos = MAP_HACK_DATA.AllTechiesMines[0][0][0][0]
			if (lastpos.Distance(position) >= 50 || center.Distance(position) >= 50) {
				MAP_HACK_DATA.ParticleSDK.SetConstrolPointsByKey(`MAP_HACK_TECHIES_${mine_name}_${lastpos.Length}`, [1, 500])
				MAP_HACK_DATA.ParticleSDK.RestartByKey(`MAP_HACK_TECHIES_${mine_name}_${lastpos.Length}`)
			}
		}

		mines.push([position, id, GameState.RawGameTime, mine_name])
		arr[1] = Vector3.GetCenter(mines.map(([vec]) => vec))
		return true
	}

	public static DeleteMineByCriteria(cb: (mine: [Vector3, number, number, string, Unit?]) => boolean) {
		MAP_HACK_DATA.AllTechiesMines.some(([positions, , name], i) => positions.some((mine, pos_id) => {
			if (!cb(mine))
				return false
			if (positions.length !== 1) {
				positions.splice(pos_id, 1)
				MAP_HACK_DATA.AllTechiesMines[i][1] = Vector3.GetCenter(MAP_HACK_DATA.AllTechiesMines[i][0].map(([vec]) => vec))
				if (AbilitiesEnumParticles.selected_id !== 0) {
					MAP_HACK_DATA.ParticleSDK.SetConstrolPointsByKey(`MAP_HACK_TECHIES_${name}_${MAP_HACK_DATA.AllTechiesMines[i][1].Length}`, [1, 425])
					MAP_HACK_DATA.ParticleSDK.RestartByKey(`MAP_HACK_TECHIES_${name}_${MAP_HACK_DATA.AllTechiesMines[i][1].Length}`)
				}
			} else {
				if (AbilitiesEnumParticles.selected_id !== 0)
					MAP_HACK_DATA.ParticleSDK.DestroyByKey(`MAP_HACK_TECHIES_${name}_${MAP_HACK_DATA.AllTechiesMines[i][1].Length}`)
				MAP_HACK_DATA.AllTechiesMines.splice(i, 1)
				MAP_HACK_DATA.AllTechiesMinesMap.delete(i)
			}
			return true
		}))
	}

	public static DeleteMineByUpdate(name: string, position: Vector3) {
		MAP_HACK_DATA.AllTechiesMines.forEach((ar, j) => {
			if (ar[2] !== name)
				return
			const mines = ar[0]
			mines.forEach(([vec], k) => {
				if (vec.Distance(position) > 10)
					return
				if (mines.length !== 1) {
					mines.splice(k, 1)
					ar[1] = Vector3.GetCenter(mines.map(([vec_]) => vec_))
				} else {
					MAP_HACK_DATA.AllTechiesMines.splice(j, 1)
					MAP_HACK_DATA.AllTechiesMinesMap.delete(j)
				}

				if (AbilitiesEnumParticles.selected_id !== 0)
					MAP_HACK_DATA.ParticleSDK.DestroyByKey(`MAP_HACK_TECHIES_${name}_${vec.Length}`)
			})
		})
	}
}
