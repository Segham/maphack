import { Ability } from "wrapper/Imports"

export class TemplarAssasinModel {
	public static MeldMap = new Map<number, [Ability, number]>()

	public static Dispose() {
		this.MeldMap.clear()
	}
}
