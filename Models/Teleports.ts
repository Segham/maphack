import { MAP_HACK_DATA } from "../data"
import { TeleportsModeParticles, TeleportsWorldParticles, TeleportsWorldScaleMiniMap, TeleportsWorldScaleWorld } from "../menu"
import { Color, GameRules, Hero, LocalPlayer, MinimapSDK, RendererSDK, Vector2, Vector3 } from "../wrapper/Imports"
import { PathX, RectangleDraw, RectangleX, UnitBarsX } from "../X-Core/Imports"

export class Teleports {

	private displayUntil = 0

	constructor(
		public hero: Hero,
		private id: number,
		private start_pos: Vector3,
		private end_pos: Vector3,
		private duration: number,
		private colorHero: Color,
	) {
		this.displayUntil = (GameRules?.RawGameTime ?? 0) + this.duration
	}

	public get IsValid() {
		return MAP_HACK_DATA.TeleportsMap.get(this.id) !== undefined && this.displayUntil >= (GameRules?.RawGameTime ?? 0)
	}

	public get RemainingDuration() {
		const modifire = this.hero.GetBuffByName("modifier_teleporting")
		this.displayUntil = modifire !== undefined ? modifire.DieTime : this.displayUntil
		return this.displayUntil - (GameRules?.RawGameTime ?? 0)
	}

	public DrawMap() {

		const ratio = (TeleportsWorldScaleWorld.value * (RendererSDK.WindowSize.y / 1080))
		this.RenderPositionWorld(RendererSDK.WorldToScreen(this.end_pos), ratio, this.colorHero)

		if (!this.hero.IsVisible)
			this.RenderPositionWorld(RendererSDK.WorldToScreen(this.start_pos), ratio, new Color(100, 100, 100))

		if (!TeleportsWorldParticles.value)
			return

		MAP_HACK_DATA.ParticleSDK.DrawLine(`TP_${this.id}`, LocalPlayer?.Hero!, this.end_pos, {
			Color: this.colorHero,
			Position: this.start_pos,
			Mode2D: 50,
			Width: 50,
		})
	}

	public DrawMiniMap() {
		const ratio = (TeleportsWorldScaleMiniMap.value * (RendererSDK.WindowSize.y / 1080))
		this.RenderPositionMiniMap(this.end_pos, ratio, this.colorHero)
		this.RenderPositionMiniMap(this.start_pos, ratio, new Color(100, 100, 100))
	}

	private RenderPositionMiniMap(position: Nullable<Vector3>, size: number, color: Color) {
		if (position === undefined || position.IsZero())
			return

		const SizeTime = Math.max(Math.min((this.RemainingDuration * 500), 2100), 900)
		MinimapSDK.DrawIcon(
			`heroicon_${this.hero.Name}`,
			position.SubtractScalar(size / 2),
			SizeTime >= 900 ? (size * 28) : SizeTime - 300,
			this.start_pos.Equals(position) ? new Color(100, 100, 100) : Color.White,
		)

		if (!this.start_pos.Equals(position))
			MinimapSDK.DrawIcon("ping_teleporting", position, SizeTime, color)
	}

	private RenderPositionWorld(position: Nullable<Vector2>, size: number, color: Color) {
		if (position === undefined || position.IsZero())
			return

		const Size = new Vector2(size, size)
		const pos = position.SubtractScalar(size / 2)

		RendererSDK.Image(
			PathX.Heroes(this.hero.Name, false),
			pos, 0,
			Size,
		)

		switch (TeleportsModeParticles.selected_id) {
			case 0:
				this.MODE_ARC(pos, Size, color)
				break;
			case 1:
				this.MODE_BAR(pos, Size, color)
				break;
			default:
				this.MODE_BAR(pos, Size, color)
				this.MODE_ARC(pos, Size, color)
				break;
		}
	}

	private MODE_ARC(pos: Vector2, Size: Vector2, color: Color) {
		RendererSDK.Image(PathX.Images.buff_outline, pos, 0, Size, Color.Black.SetA(200))
		const size = Size.SubtractScalar(5)
		const position = pos.AddScalar(3)
		const timetoshow = 100 * this.RemainingDuration / this.duration
		RendererSDK.Arc(-90, timetoshow, position, size, true, 1, Color.Black.SetA(150))
		RendererSDK.Arc(-90, timetoshow, position, size, false, 3, color.SetA(200))
		this.TextRender(pos, Size)
	}

	private MODE_BAR(pos: Vector2, Size: Vector2, color: Color) {
		RendererSDK.Image(PathX.Images.buff_outline, pos, 0, Size, color)
		const bars = new UnitBarsX(this.end_pos.Clone())
		const MPPosition = bars.HealthBarPosition
		if (MPPosition.IsZero())
			return
		const manaBarSize = bars.HealthBarSize
		const vector = new Vector2(manaBarSize.x, Math.floor(manaBarSize.y * 0.53)).AddForThis(new Vector2(0, 10))
		const location = MPPosition.AddForThis(new Vector2(0, manaBarSize.y + 1))
		if (vector.y <= 0)
			return
		const Rectangle = new RectangleX(location, vector.Clone()).AddSizeVector(1.5)
		const Rectangle2 = new RectangleX(location, new Vector2(vector.x * this.RemainingDuration / this.duration, vector.y))
		RectangleDraw.FilledRect(Rectangle, Color.Black)
		RectangleDraw.Image(PathX.Images.topbar_mana, Rectangle2, new Color(153, 188, 198))
		this.TextRender(Rectangle.Position, Rectangle.Size)
	}

	private TextRender(pos: Vector2, Size: Vector2) {
		const pos_ = Vector2.FromVector3(RendererSDK.GetTextSize(this.RemainingDuration.toFixed(1), "Calibri", 18))
			.MultiplyScalarForThis(-1)
			.AddScalarX(Size.x)
			.AddScalarY(Size.y)
			.DivideScalarForThis(2)
			.AddScalarX(pos.x)
			.AddScalarY(pos.y)
			.RoundForThis()
		RendererSDK.Text(this.RemainingDuration.toFixed(1), pos_, Color.White, "Calibri", 18)
	}
}
