import { Ability, antimage_blink, Color, Creep, Hero, MinimapSDK, npc_dota_hero_furion, npc_dota_hero_nevermore, queenofpain_blink, RendererSDK, Roshan, SpiritBear, Team, Unit, Vector2, Vector3 } from "gitlab.com/FNT_Rework/wrapper/wrapper/Imports"
import { PathX, PlayerX, RectangleDraw, RectangleX } from "gitlab.com/FNT_Rework/x-core/Imports"
import { MAP_HACK_DATA } from "../data"
import { AbilitiesMiniMap, AbilitiesScaleMiniMap, AbilitiesScaleWorld, AbilitiesWorld } from "../menu"
import { AbilitiesParticles } from "../Modules/Abilities/AbilitiesData"

export class AbilitesModel {

	public static DrawBigImage(path: string, abil: Nullable<Ability>, ent: Unit, pos: Vector3, id: number) {

		if (!AbilitiesWorld.value || abil === undefined)
			return

		const owner = ent ?? abil?.Owner

		if (owner === undefined || owner.IsIllusion || owner === undefined || !owner.IsValid || !owner.IsEnemy())
			return

		const pcolor = (PlayerX.ColorX.get((owner as Hero).PlayerID) ?? Color.Red).SetA(230)

		if (MAP_HACK_DATA.ParticleSDK.AllParticles.has(`MAP_HACK_DRAW_RANGE_${id}`))
			MAP_HACK_DATA.ParticleSDK.SetConstrolPointsByKey(`MAP_HACK_DRAW_RANGE_${id}`, [2, pcolor])

		const screen = RendererSDK.WorldToScreen(pos)
		if (screen === undefined || owner.IsVisible && !(abil instanceof antimage_blink || abil instanceof queenofpain_blink) || !owner.IsAlive)
			return

		let Size = (AbilitiesScaleWorld.value * (RendererSDK.WindowSize.y / 1080))
		if (path.includes("_start"))
			Size = Size / 1.3
		const position = new RectangleX(screen, new Vector2(Size, Size))

		if (position.IsZero())
			return

		let Image = owner.Name

		if (owner instanceof Creep && owner.IsLaneCreep)
			Image = owner.Team === Team.Dire ? "npc_dota_hero_creep_radiant" : "npc_dota_hero_creep_dire"

		if (owner instanceof Creep && !owner.IsLaneCreep && owner.IsSummoned)
			Image = owner.Name.includes("npc_dota_lycan_wolf") ? "npc_dota_lycan_wolf" : owner.Name

		if (path === "particles/units/heroes/hero_gyrocopter/gyro_guided_missile.vpcf")
			Image = "npc_dota_hero_gyrocopter"

		if (owner instanceof SpiritBear)
			Image = `npc_dota_lone_druid_bear`

		const outline = position.Clone().MultiplySizeFX(1.2)
		RectangleDraw.Image(PathX.Images.buff_outline, outline, path.includes("blink_start") ? pcolor.SetA(165) : pcolor)

		RectangleDraw.Image((owner instanceof Roshan
			? PathX.Images.roshan_halloween_angry
			: PathX.Heroes(Image, false)),
			position,
			(path.includes("blink_start") ? Color.White.SetA(165) : Color.White), 0,
		)

		const abilPos = position.Clone().MultiplySizeFX(0.5)
		abilPos.X += abilPos.Width * 0.8
		abilPos.Y += abilPos.Height * 0.6
		const outline2 = abilPos.Clone().MultiplySizeFX(1.2)

		// TODO => transfer map && check aura
		if (owner instanceof npc_dota_hero_furion || owner instanceof npc_dota_hero_nevermore) {
			// const isBuffBlink = owner.HasBuffByName("modifier_item_arcane_blink_buff")
			let time = 0
			if (owner instanceof npc_dota_hero_furion)
				time = 3
			if (owner instanceof npc_dota_hero_nevermore)
				time = 1.6
			RectangleDraw.Arc(
				-90,
				MAP_HACK_DATA.GameSleeper.RemainingSleepTime(`SLEEP_ABILITY_MAP_HACK_${id}`) / 1000,
				time,
				true,
				position,
				path.includes("furion_teleport.vpcf") ? Color.Red : Color.Black,
			)
		}

		RectangleDraw.Image(
			!abil.IsItem ? PathX.DOTAAbilities(abil.Name) : PathX.DOTAItems(abil.Name),
			abilPos,
			(path.includes("blink_start") ? Color.White.SetA(165) : Color.White.SetA(230)), 0,
		)

		RectangleDraw.Image(
			PathX.Images.buff_outline,
			outline2,
			(path.includes("blink_start") ? Color.White.SetA(165) : Color.Red.SetA(230)),
		)
	}

	public static DrawMiniMapImage(path: string, abil: Nullable<Ability>, ent: Unit, pos: Vector3) {
		if (!AbilitiesMiniMap.value || abil === undefined)
			return
		const owner = ent ?? abil.Owner
		if (!(owner instanceof Hero))
			return

		const data = AbilitiesParticles.find(x => x.path.includes(path))
		if (data === undefined)
			return

		if (data.path === "particles/units/heroes/hero_arc_warden/arc_warden_wraith.vpcf")
			return

		let Size = AbilitiesScaleMiniMap.value * 100
		const color = (path.includes("blink_start") ? Color.White.SetA(165) : Color.White)
		if (path.includes("_start"))
			Size = Size / 1.3

		MinimapSDK.DrawIcon(`heroicon_${owner.Name}`, pos, Size, color)
	}

	public static RemoveParticles(ids: number) {
		MAP_HACK_DATA.ParticleSDK.DestroyByKey(`MAP_HACK_DRAW_RANGE_${ids}`)
		MAP_HACK_DATA.ParticleSDK.DestroyByKey(`MAP_HACK_DRAW_REACTAGLE_${ids}`)
		MAP_HACK_DATA.ParticleSDK.DestroyByKey(`MAP_HACK_DRAW_RANGE_TIMER_${ids}`)
	}
}
