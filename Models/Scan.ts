import { PathX } from "gitlab.com/FNT_Rework/x-core/Imports";
import { Color, DOTAGameUIState_t, GameState, Notification, Rectangle, RendererSDK } from "wrapper/Imports";

export class NotificationScan extends Notification {

	constructor(sound: string, volume: number) {
		super({
			timeToShow: 6 * 1000,
			playSound: sound,
			playVolume: volume,
		})
	}

	public OnClick(): boolean {
		return false
	}

	public Draw(position: Rectangle): void {
		if (GameState.UIState !== DOTAGameUIState_t.DOTA_GAME_UI_DOTA_INGAME)
			return
		const notificationSize = this.GetNotificationSize(position)
		const textureSize = this.GetTextureSize(notificationSize)
		const opacityWhite = Color.White.SetA(this.Opacity)
		RendererSDK.Image(
			PathX.Images.bg_deathsummary,
			notificationSize.pos1,
			-1,
			notificationSize.Size,
			opacityWhite,
		)
		RendererSDK.Image(
			PathX.Images.icon_scan,
			textureSize.pos1,
			-1,
			textureSize.Size,
			opacityWhite,
		)
	}

	private GetTextureSize(position: Rectangle) {
		const result = position.Clone()
		result.Width = position.Width * 0.5
		result.Height = position.Height
		result.x = position.x + position.Width / 2 - result.Width / 2
		result.y = position.y
		return result
	}

	private GetNotificationSize(position: Rectangle) {
		const result = position.Clone()
		result.x = position.x + position.Width / 2
		result.Width = position.Width * 0.5
		return result
	}
}
