import { Vector3 } from "wrapper/Imports"

export class VectorX {
	public static Multiply(num: number, multiply: Vector3) {
		return new Vector3(multiply.x * num, multiply.y * num, multiply.z * num)
	}
}
