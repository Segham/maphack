import { Ability, Entity, EntityManager, Hero } from "gitlab.com/FNT_Rework/wrapper/wrapper/Imports";
import { State } from "../menu";
import { GameX } from "../X-Core/Imports";

export class MAP_HACK_VALIDATE {
	public static get IsInGame() {
		return GameX.IsInGame && State.value
	}
	public static EntityVerificaton(unit: Entity | string, abilities: Ability | string) {
		let hero: Nullable<Hero>
		let abil: Nullable<Ability>

		if (typeof unit === "string")
			hero = EntityManager.GetEntitiesByClass(Hero).find(x => !x.IsIllusion && x.Name === unit)

		if (unit instanceof Hero)
			hero = unit

		if (hero === undefined || !hero.IsEnemy())
			return undefined

		if (typeof abilities === "string")
			abil = hero.GetAbilityByName(abilities)

		if (abilities instanceof Ability)
			abil = abilities

		if (abil === undefined || abil.Owner === undefined || !abil.Owner.IsEnemy())
			return undefined

		return { Ability: abil, Hero: hero }
	}
}
