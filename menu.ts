import { Color, Menu as MenuSDK } from "./wrapper/Imports";

const Menu = MenuSDK.AddEntryDeep(["Visual", "MapHack"])
export const State = Menu.AddToggle("State", true)

/** @JUNGLE_ATTACK_CREEPS */
const JungleMapTree = Menu.AddNode("Jungle")
export const JungleMapState = JungleMapTree.AddToggle("State", true, "Monitoring who kill jungle creeps")
export const JungleMapIconSize = JungleMapTree.AddSlider("Size on minimap", 64, 24, 150, 0, "Size hero icons minimap")
export const JungleMapImageSizeWorld = JungleMapTree.AddSlider("Size in world", 40, 20, 100, 0, "Size hero icons world")
export const JungleMapImageMode = JungleMapTree.AddDropdown("Display mode", ["Round icons", "Mini icons"], 0, "Icons display mode in world")

/** @JUNGLE_CREEPS */
const JungleCreepMapTree = Menu.AddNode("Camps")
export const JungleCreepMapState = JungleCreepMapTree.AddToggle("State")
export const JungleCreepMapGold = JungleCreepMapTree.AddToggle("Show gold", true, "Average gold value when killing creeps")

/** @TELEPOST */
const TeleportsTree = Menu.AddNode("Teleports")
export const TeleportsState = TeleportsTree.AddToggle("State", true)
export const TeleportsWorld = TeleportsTree.AddToggle("Show in world", true)
export const TeleportsMiniMap = TeleportsTree.AddToggle("Show on minimap", true)
export const TeleportsWorldParticles = TeleportsTree.AddToggle("Show line in world", true)
export const TeleportsWorldScaleMiniMap = TeleportsTree.AddSlider("Size on minimap", 25, 25, 100, 0, "Size hero icons minimap")
export const TeleportsWorldScaleWorld = TeleportsTree.AddSlider("Size in world", 60, 20, 100, 0, "Size hero icons world")

export const TeleportsModeParticles = TeleportsTree.AddDropdown("Display mode", ["Arc", "Bar", "Arc & Bar"])

const AbilitiesTree = Menu.AddNode("Abilities")
export const AbilitiesState = AbilitiesTree.AddToggle("State", true)
export const AbilitiesWorld = AbilitiesTree.AddToggle("Show in world", true)
export const AbilitiesMiniMap = AbilitiesTree.AddToggle("Show on minimap", true)
export const AbilitiesScaleWorld = AbilitiesTree.AddSlider("Size in world", 40, 20, 100, 0, "Size hero icons world")
export const AbilitiesScaleMiniMap = AbilitiesTree.AddSlider("Size on minimap", 8, 3, 20, 0, "Size hero icons minimap")
export const AbilitiesEnumParticles = AbilitiesTree.AddDropdown("Radius particle model", ["Disable", "Normal", "Rope", "Animated"], 1)

const WispTree = Menu.AddNode("Wisp")
export const WispState = WispTree.AddToggle("State", true)
export const WispWorld = WispTree.AddToggle("Show in world", true)
export const WispMiniMap = WispTree.AddToggle("Show on minimap", true)
export const WispScaleMiniMap = WispTree.AddSlider("Size on minimap", 8, 3, 20, 0, "Size hero icons minimap")
export const WispScaleWorld = WispTree.AddSlider("Size in world", 40, 20, 100, 0, "Size hero icons world")

const PredictionTree = Menu.AddNode("Prediction")
const PredictionHeroTree = PredictionTree.AddNode("Heroes")
export const PredictionHeroState = PredictionHeroTree.AddToggle("State")
export const PredictionHeroWorld = PredictionHeroTree.AddToggle("Show in world", true)
export const PredictionHeroMiniMap = PredictionHeroTree.AddToggle("Show on minimap", true)
export const PredictionHeroScaleMiniMap = PredictionHeroTree.AddSlider("Size on minimap", 8, 3, 20, 0, "Size hero icons minimap")
export const PredictionHeroScaleWorld = PredictionHeroTree.AddSlider("Size in world", 40, 20, 100, 0, "Size hero icons world")

const PredictionWardsTree = PredictionTree.AddNode("Wards")
export const PredictionWardsState = PredictionWardsTree.AddToggle("State", true)
export const PredictionWardsMiniMap = PredictionWardsTree.AddToggle("Show on minimap", true)
export const PredictionWardsWorld = PredictionHeroTree.AddToggle("Show in world", true)
export const PredictionWardsCooldown = PredictionWardsTree.AddToggle("Remaining time", true, "Show remaining ward time")
export const PredictionWardsPingAllies = PredictionWardsTree.AddToggle("Ping for allies", false, "Ping to the location of an enemy ward for allies")
export const PredictionWardsTypeRender = PredictionWardsTree.AddDropdown("Mode type", ["2D", "3D", "Round image"])

const PredictionWardsParicleTree = PredictionWardsTree.AddNode("Radiuses setting")
export const PredictionWardsParicleState = PredictionWardsParicleTree.AddToggle("State", true)
export const PredictionWardsParicleStyle = PredictionWardsParicleTree.AddDropdown("Style", ["Normal", "Rope", "Animation"], 2, "Change only applies after placing new ward")
export const PredictionWardsParicleWidth = PredictionWardsParicleTree.AddSlider("Width", 15, 1, 150, 0, "Particle width")

export const PredictionWardsParicleColorSentry = PredictionWardsTree.AddColorPicker("Sentry color", new Color(15, 0, 221))
export const PredictionWardsParicleColorObserver = PredictionWardsTree.AddColorPicker("Observer color", new Color(222, 170, 0))

const PredictionCreepsTree = PredictionTree.AddNode("Creeps")
export const PredictionCreepsState = PredictionCreepsTree.AddToggle("State", true)
export const PredictionCreepsoWorld = PredictionCreepsTree.AddToggle("Show in world", true)
export const PredictionCreepsMiniMap = PredictionCreepsTree.AddToggle("Show on minimap", true)
export const PredictionCreepsScaleMiniMap = PredictionCreepsTree.AddSlider("Size on minimap", 10, 3, 20, 0, "Size creep icons minimap")
export const PredictionCreepsScaleWorld = PredictionCreepsTree.AddSlider("Size in world", 22, 20, 100, 0, "Size text icons world")
export const PredictionCreepsMninColor = PredictionCreepsTree.AddColorPicker("Icons color", Color.Aqua, "Icons color on minimap")
export const PredictionCreepsWorldColor = PredictionCreepsTree.AddColorPicker("Text color", Color.White, "Text color in world")

const PredictionCouriersTree = PredictionTree.AddNode("Couriers")
export const PredictionCouriersState = PredictionCouriersTree.AddToggle("State", true)
export const PredictionCouriersWorld = PredictionCouriersTree.AddToggle("Show in world", true)
export const PredictionCouriersMiniMap = PredictionCouriersTree.AddToggle("Show on minimap", true)
export const PredictionCouriersScaleMiniMap = PredictionCouriersTree.AddSlider("Size on minimap", 8, 3, 20, 0, "Size couriers icons minimap")
export const PredictionCouriersScaleWorld = PredictionCouriersTree.AddSlider("Size in world", 50, 20, 100, 0, "Size couriers icons world")

const ScanTree = Menu.AddNode("Scan")
export const ScanState = ScanTree.AddToggle("State", true)
export const ScanWorld = ScanTree.AddToggle("Show in world", true)
export const ScanMiniMap = ScanTree.AddToggle("Show on minimap", true)
export const ScanScaleMiniMap = ScanTree.AddSlider("Size on minimap", 9, 4, 20, 0, "Size icon minimap")
export const ScanScaleWorld = ScanTree.AddSlider("Size in world", 40, 20, 100, 0, "Size icon world")
export const ScanSoundVolunme = ScanTree.AddSlider("Volume %", 2, 0, 100, 0)

MenuSDK.Localization.AddLocalizationUnit("russian", new Map([
	["Wisp", "Висп"],
	["Scan", "Радар"],
	["Camps", "Лагеря"],
	["Rope", "Верёвка"],
	["Jungle", "Лесной"],
	["MapHack", "МапХак"],
	["Normal", "Обычный"],
	["Wards", "Варды"],
	["Creeps", "Крипы"],
	["Disable", "Отключить"],
	["Ward time", "Время варда"],
	["Mode type", "Тип отрисовки"],
	["Round image", "Круглое изображение"],
	["Particle width", "Ширина партикля"],
	["Radiuses setting", "Настройка радиусов"],
	["Sentry color", "Цвет сентри"],
	["Observer color", "Цвет обсервера"],
	["Prediction", "Предугадывание"],
	["Couriers", "Курьеры"],
	["Show gold", "Показать золото"],
	["Teleports", "Телепорты"],
	["Animated", "Анимированый"],
	["Mini icons", "Мини иконки"],
	["Round icons", "Круглые иконки"],
	["Icons color", "Цвет иконок"],
	["Text color", "Цвет текста"],
	["Radius color", "Цвет радиуса"],
	["Vision radius", "Радиус варда"],
	["Icons color on minimap", "Цвет иконок на миникарте"],
	["Text color in world", "Цвет текста в мире"],
	["Display mode", "Режим отображения"],
	["Radius particle model", "Модель радиуса"],
	["Show on minimap", "Показывать на миникарте"],
	["Size hero icons world", "Размер иконок героев в мире"],
	["Size couriers icons world", "Размер иконок курьеров в мире"],
	["Size in world", "Размер в мире"],
	["Show line in world", "Показывать линию в мире"],
	["Use size icon dota", "Использовать настройки доты"],
	["Show in world", "Показывать в мире"],
	["Color hero icon", "Цвет иконки героя"],
	["Size on minimap", "Размер на миникарте"],
	["Ping for allies", "Пинг для союзников"],
	["Remaining time", "Оставшееся время"],
	["Size text icons world", "Размер текста в мире"],
	["Size creep icons minimap", "Размер иконок крипов"],
	["Enemy team used smoke", "Вражеская команда использовала смок"],
	["Size hero icons minimap", "Размер иконок героев на миникарте"],
	["Size couriers icons minimap", "Размер иконок курьеров на миникарте"],
	["Icons display mode in world", "Режим отображения иконок в мире"],
	["Show remaining ward time", "Показать оставшееся время варда"],
	["Monitoring who kill jungle creeps", "Наблюдение за тем, кто убивает крипов в лесу"],
	["Average gold value when killing creeps", "Среднее значение золота при убийстве крипов"],
	["Ping to the location of an enemy ward for allies", "Пинговать в место установки вражеского варда для союзников"],
	["Change only applies after placing new ward", "Изменение применяется только после установки нового варда"],
]))
