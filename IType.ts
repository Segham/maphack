import { Ability, Color, Entity, Item, Vector3 } from "wrapper/Imports"

export type TeleportsMap = Map<number, [
	string,
	boolean,
	Vector3?,
	Vector3?,
	Entity?,
	Color?
]>

export type PudgeHookMap = Map<number, [
	string, /* path */
	Vector3, /* start */
	Vector3,  /* end */
	number, /* start game time */
	number, /* speed */
]>

export type WardDispenserCount = Map<number, [
	number, /** sentry count */
	number  /** observer count */
]>

export type IceBlastMap = Map<number, [number,
	Vector3,
	Vector3,
	Vector3
]>
export type ItemsMap = Map<number, [
	string,
	Nullable<Entity>,
	number,
	number,
	Vector3?,
	Item?,
]>

export type AbilitiesMap = Map<number, [
	string,
	Ability,
	Nullable<Entity | number>,
	Vector3?,
	Vector3?,
	number?,
	boolean?,
	Entity?,
	boolean?,
	Color?
]>
