import { DOTAGameUIState_t, EventsSDK, GameState } from "wrapper/Imports";
import { ABILITIES_DRAW } from "../Modules/Abilities";
import { MAP_HACK_CAMP_DRAW } from "../Modules/Camp";
import { MAP_HACK_ABILITIES_ENTITIES_DRAW } from "../Modules/Entities";
import { ITEMS_DRAW } from "../Modules/Items";
import { JUNGLE_MAP_HACK_DRAW } from "../Modules/Jungle";
import { PREDICTION_COURIERS_DRAW } from "../Modules/Prediction/Courier/index";
import { CREEP_WAVE_DRAW } from "../Modules/Prediction/Creeps/index";
import { PREDICTION_HEROES_DRAW } from "../Modules/Prediction/Hero/index";
import { MAP_HACK_WARD_TRACKER_DRAW } from "../Modules/Prediction/Wards/index";
import { ABILITIES_LINEAR_PROJECTILE_DRAW } from "../Modules/Projectile";
import { SCAN_MODIFIER_DRAW } from "../Modules/Scan";
import { MAP_HACK_TECHIES_DRAW } from "../Modules/Techies";
import { TELEPORTS_DRAW } from "../Modules/Teleports";
import { ABILITIES_TA_DRAW } from "../Modules/Templar";
import { ABILITIES_MODIFIER_DRAW } from "../Modules/Thinker";
import { MAP_HACK_WISP_DRAW } from "../Modules/Wisp";
import { MAP_HACK_VALIDATE } from "../Service/Validate";

EventsSDK.on("Draw", () => {
	if (!MAP_HACK_VALIDATE.IsInGame || GameState.UIState !== DOTAGameUIState_t.DOTA_GAME_UI_DOTA_INGAME)
		return
	ITEMS_DRAW()
	TELEPORTS_DRAW()
	ABILITIES_DRAW()
	CREEP_WAVE_DRAW()
	ABILITIES_TA_DRAW()
	SCAN_MODIFIER_DRAW()
	MAP_HACK_WISP_DRAW()
	MAP_HACK_CAMP_DRAW()
	JUNGLE_MAP_HACK_DRAW()
	MAP_HACK_TECHIES_DRAW()
	PREDICTION_HEROES_DRAW()
	ABILITIES_MODIFIER_DRAW()
	PREDICTION_COURIERS_DRAW()
	MAP_HACK_WARD_TRACKER_DRAW()
	ABILITIES_LINEAR_PROJECTILE_DRAW()
	MAP_HACK_ABILITIES_ENTITIES_DRAW()
})
