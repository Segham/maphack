import { EventsSDK } from "wrapper/Imports";
import { ABILITIES_TA_ANIM } from "../Modules/Templar";
import { MAP_HACK_VALIDATE } from "../Service/Validate";

EventsSDK.on("UnitAnimation", (npc, a, b, c, d, acitivity) => {
	if (!MAP_HACK_VALIDATE.IsInGame)
		return
	ABILITIES_TA_ANIM(npc, acitivity)
})
