import { EventEmitter, WardObserver, WardTrueSight } from "wrapper/Imports"
import { IDATAWards } from "../../data"

export const EventMapHack: EventMapHack = new EventEmitter()

export interface EventMapHack extends EventEmitter {
	on(name: "RemoveWard", callback: (data: IDATAWards) => void): EventEmitter
}

export interface EventMapHack extends EventEmitter {
	on(name: "SetWard", callback: (ent: WardObserver | WardTrueSight) => void): EventEmitter
}
