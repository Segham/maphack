import { ArrayExtensions, GameState, WardObserver, WardTrueSight } from "gitlab.com/FNT_Rework/wrapper/wrapper/Imports"
import { MAP_HACK_DATA } from "../../data"
import { PredictionWardsParicleState } from "../../menu"
import { WardModel } from "../../Models/Wards"
import { EventMapHack } from "./Interface"

export function EmiterTimeWard() {
	const ward = MAP_HACK_DATA.WardsData.find(data => data !== undefined
		&& data.time <= GameState.RawGameTime)
	if (ward === undefined)
		return
	EventMapHack.emit("RemoveWard", false, ward)
}

EventMapHack.on("SetWard", data => {
	const is_observer = data instanceof WardObserver && data.Name === "npc_dota_observer_wards"
	const is_sentry = data instanceof WardTrueSight && data.Name === "npc_dota_sentry_wards"
	if (!is_observer && !is_sentry)
		return

	const nearest_ward = ArrayExtensions.orderBy(MAP_HACK_DATA.WardsData.filter(ward =>
		(is_observer && ward.type === "observer") || (is_sentry && ward.type === "sentry")),
		ward => data.Distance(ward.position))[0]

	if (nearest_ward === undefined || data.Distance2D(nearest_ward.position) > 600) {
		WardModel.VisionWardCreated(data, is_observer)
		return
	}

	const time = data.GetBuffByName("modifier_item_buff_ward")?.DieTime ?? nearest_ward.time
	nearest_ward.time = time
	nearest_ward.position = data.Position
	MAP_HACK_DATA.ParticleSDK.SetConstrolPointsByKey(nearest_ward.particleID + "map_ping", [0, data.Position])
	MAP_HACK_DATA.ParticleSDK.SetConstrolPointsByKey(nearest_ward.particleID + "map_model", [2, data.Position])
	MAP_HACK_DATA.ParticleSDK.SetConstrolPointsByKey(nearest_ward.particleID + nearest_ward.type, [0, data.Position])
})

EventMapHack.on("RemoveWard", data => {

	if (PredictionWardsParicleState.value)
		WardModel.RemoveParticle(data, data.type)

	WardModel.RemoveParticle(data, "map_ping")
	WardModel.RemoveParticle(data, "map_model")
	ArrayExtensions.arrayRemove(MAP_HACK_DATA.WardsData, data)
})
