import { EventsSDK } from "wrapper/Imports";
import { MAP_HACK_SOUND } from "../Modules/Entities";
import { MAP_HACK_VALIDATE } from "../Service/Validate";

EventsSDK.on("StartSound", (name, source_ent, position) => {
	if (!MAP_HACK_VALIDATE.IsInGame)
		return
	MAP_HACK_SOUND(name, source_ent, position)
})
