import { MAP_HACK_DATA } from "../data";
import { AbilitiesState, AbilitiesWorld, PredictionCreepsState, PredictionWardsParicleColorObserver, PredictionWardsParicleColorSentry, PredictionWardsParicleWidth, PredictionWardsState, PredictionWardsWorld, State } from "../menu";
import { TemplarAssasinModel } from "../Models/Templar";
import { WardModel } from "../Models/Wards";
import { WispModel } from "../Models/Wisp";

State.OnDeactivate(() =>
	MAP_HACK_DATA.Dispose(),
)

AbilitiesWorld.OnDeactivate(() => {
	MAP_HACK_DATA.IceBlastFinal = false
	MAP_HACK_DATA.IceBlastMap.clear()
	MAP_HACK_DATA.AbilitiesMap.clear()
	MAP_HACK_DATA.ParticleSDK.DestroyAll(true)
})

PredictionCreepsState.OnDeactivate(() => {
	MAP_HACK_DATA.CreepWaves = []
	MAP_HACK_DATA.CreepWaveIndex = 0
	MAP_HACK_DATA.lanePaths.Lanes.clear()
	MAP_HACK_DATA.GameSleeper.ResetKey("MAP_HACK_SPAWN_CREEPS")
})

AbilitiesState.OnDeactivate(() => {
	WispModel.Dispose()
	TemplarAssasinModel.Dispose()
	MAP_HACK_DATA.AbilitiesMap.clear()
	MAP_HACK_DATA.ModifierTime.clear()
	MAP_HACK_DATA.GameSleeper.FullReset()
	MAP_HACK_DATA.IceBlastFinal = false
	MAP_HACK_DATA.IceBlastMap.clear()
	MAP_HACK_DATA.ParticleSDK.DestroyAll(true)
})

PredictionWardsWorld.OnDeactivate(() =>
	MAP_HACK_DATA.ParticleSDK.DestroyAll(true))

PredictionWardsState.OnDeactivate(() =>
	WardModel.Dispose(),
)

PredictionWardsParicleWidth.OnValue(call => {
	MAP_HACK_DATA.WardsData.forEach(ids => {
		const uniqID = ids.particleID + ids.type
		const ID = MAP_HACK_DATA.ParticleSDK.AllParticles.get(uniqID)
		if (ID === undefined)
			return
		ID.SetControlPoints([3, call.value])
	})
})

PredictionWardsParicleColorObserver.OnValue(call => {
	MAP_HACK_DATA.WardsData.forEach(ids => {
		if (ids.type !== "observer")
			return
		const uniqID = ids.particleID + ids.type
		const ID = MAP_HACK_DATA.ParticleSDK.AllParticles.get(uniqID)
		if (ID === undefined)
			return
		ID.SetControlPoints([2, call.selected_color])
	})
})

PredictionWardsParicleColorSentry.OnValue(call => {
	MAP_HACK_DATA.WardsData.forEach(ids => {
		if (ids.type !== "sentry")
			return
		const uniqID = ids.particleID + ids.type
		const ID = MAP_HACK_DATA.ParticleSDK.AllParticles.get(uniqID)
		if (ID === undefined)
			return
		ID.SetControlPoints([2, call.selected_color])
	})
})
