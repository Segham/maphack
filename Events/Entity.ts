import { Ability, ArrayExtensions, EventsSDK } from "wrapper/Imports";
import { MAP_HACK_DATA } from "../data";
import { AbilitiesPhase } from "../Modules/Abilities/ProjectileData"
import { MAP_HACK_ABILITIES_ENTITIES_CREATED, MAP_HACK_ABILITIES_ENTITIES_DESTROY } from "../Modules/Entities";
import { CREEP_WAVE_ENTITY_CREATE, CREEP_WAVE_ENTITY_DESTROY } from "../Modules/Prediction/Creeps/index";
import { MAP_HACK_WARD_TRACKER_ENTITY_CREATED } from "../Modules/Prediction/Wards/index";
import { MAP_HACK_TECHIES_ENTITY_DESTROY } from "../Modules/Techies";

EventsSDK.on("EntityCreated", ent => {

	CREEP_WAVE_ENTITY_CREATE(ent)
	MAP_HACK_ABILITIES_ENTITIES_CREATED(ent)
	MAP_HACK_WARD_TRACKER_ENTITY_CREATED(ent)

	if (ent instanceof Ability && AbilitiesPhase.some(class_ => ent instanceof class_) && !MAP_HACK_DATA.AbilitiesList.includes(ent))
		MAP_HACK_DATA.AbilitiesList.push(ent)
})

EventsSDK.on("EntityDestroyed", ent => {

	CREEP_WAVE_ENTITY_DESTROY(ent)
	MAP_HACK_TECHIES_ENTITY_DESTROY(ent)
	MAP_HACK_ABILITIES_ENTITIES_DESTROY(ent)

	if (ent instanceof Ability)
		ArrayExtensions.arrayRemove(MAP_HACK_DATA.AbilitiesList, ent)
})
