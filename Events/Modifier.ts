import { EventsSDK } from "wrapper/Imports";
import { SCAN_MODIFIER_CREATE, SCAN_MODIFIER_DESTROY } from "../Modules/Scan";
import { ABILITIES_MODIFIER_CREATE, ABILITIES_MODIFIER_DESTROY } from "../Modules/Thinker";
import { MAP_HACK_WISP_MODFIRE_DESTROY } from "../Modules/Wisp";
import { MAP_HACK_VALIDATE } from "../Service/Validate";
EventsSDK.on("ModifierCreated", modifier => {
	if (!MAP_HACK_VALIDATE.IsInGame)
		return
	SCAN_MODIFIER_CREATE(modifier)
	ABILITIES_MODIFIER_CREATE(modifier)
})

EventsSDK.on("ModifierRemoved", modifier => {
	if (!MAP_HACK_VALIDATE.IsInGame)
		return
	SCAN_MODIFIER_DESTROY(modifier)
	ABILITIES_MODIFIER_DESTROY(modifier)
	MAP_HACK_WISP_MODFIRE_DESTROY(modifier)
})
