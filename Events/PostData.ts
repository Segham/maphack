import { EventsSDK } from "wrapper/Imports";
import { CREEP_WAVE_POST_UPDATE } from "../Modules/Prediction/Creeps/index";
import { MAP_HACK_VALIDATE } from "../Service/Validate";

EventsSDK.on("PostDataUpdate", () => {
	if (!MAP_HACK_VALIDATE.IsInGame)
		return
	CREEP_WAVE_POST_UPDATE()
})
