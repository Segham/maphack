import {  EventsSDK } from "wrapper/Imports";
import { ABILITIES_CREATE, ABILITIES_DESTROYED, ABILITIES_RELEASED, ABILITIES_UPDATED, ABILITIES_UPDATED_ENT } from "../Modules/Abilities";
import { ITEMS_CREATE, ITEMS_UPDATED, ITEMS_UPDATED_ENT } from "../Modules/Items"
import { MAP_HACK_WARD_TRACKER_PARTICLE_CREATED } from "../Modules/Prediction/Wards/index";
import { ABILITIES_PARTICLES_PROJ_CREATE, ABILITIES_PARTICLES_PROJ_DESTROY, ABILITIES_PARTICLES_PROJ_UPDATE, ABILITIES_PARTICLES_PROJ_UPDATE_ENT } from "../Modules/Projectile";
import { MAP_HACK_TECHIES_PARTICLES_CREATED, MAP_HACK_TECHIES_PARTICLES_DESTROY, MAP_HACK_TECHIES_PARTICLES_UPDATE, MAP_HACK_TECHIES_PARTICLES_UPDATE_ENT } from "../Modules/Techies";
import { TELEPORTS_CREATE, TELEPORTS_DESTROY, TELEPORTS_UPDATED, TELEPORTS_UPDATED_ENT } from "../Modules/Teleports";
import { MAP_HACK_WISP_PARTICLE_CREATED, MAP_HACK_WISP_PARTICLE_UPDATE_ENT } from "../Modules/Wisp";

import { MAP_HACK_VALIDATE } from "../Service/Validate";

EventsSDK.on("ParticleCreated", (id, path, a, b, ent) => {
	if (!MAP_HACK_VALIDATE.IsInGame)
		return
	ITEMS_CREATE(id, path, ent)
	TELEPORTS_CREATE(id, path)
	ABILITIES_CREATE(id, path, ent)
	MAP_HACK_WISP_PARTICLE_CREATED(id, a)
	ABILITIES_PARTICLES_PROJ_CREATE(id, path)
	MAP_HACK_TECHIES_PARTICLES_CREATED(id, path, ent)
	MAP_HACK_WARD_TRACKER_PARTICLE_CREATED(path, ent)
})

EventsSDK.on("ParticleUpdatedEnt", (id, controlPoint, ent, a, b, position) => {
	if (!MAP_HACK_VALIDATE.IsInGame)
		return
	TELEPORTS_UPDATED_ENT(id, position)
	MAP_HACK_WISP_PARTICLE_UPDATE_ENT(id, ent, position)
	ITEMS_UPDATED_ENT(id, controlPoint, ent, position)
	ABILITIES_UPDATED_ENT(id, controlPoint, ent, position)
	ABILITIES_PARTICLES_PROJ_UPDATE_ENT(id, controlPoint, ent, position)
	MAP_HACK_TECHIES_PARTICLES_UPDATE_ENT(id, controlPoint, ent, position)
})

EventsSDK.on("ParticleUpdated", (id, controlPoint, position) => {
	if (!MAP_HACK_VALIDATE.IsInGame)
		return
	ITEMS_UPDATED(id, controlPoint, position)
	ABILITIES_UPDATED(id, controlPoint, position)
	TELEPORTS_UPDATED(id, controlPoint, position)
	ABILITIES_PARTICLES_PROJ_UPDATE(id, controlPoint, position)
	MAP_HACK_TECHIES_PARTICLES_UPDATE(id, controlPoint, position)
})

EventsSDK.on("ParticleDestroyed", id => {
	TELEPORTS_DESTROY(id)
	ABILITIES_DESTROYED(id)
	ABILITIES_PARTICLES_PROJ_DESTROY(id)
	MAP_HACK_TECHIES_PARTICLES_DESTROY(id)
})

EventsSDK.on("ParticleReleased", id => {
	ABILITIES_RELEASED(id)
})
