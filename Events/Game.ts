import { EventsSDK } from "wrapper/Imports";
import { MAP_HACK_ABILITIES_ENTITIES_GAME_DESTROY } from "../Modules/Entities";
import { JUNGLE_MAP_HACK_GAME } from "../Modules/Jungle";
import { MAP_HACK_WARD_TRACKER_GAME_EVENT } from "../Modules/Prediction/Wards/index";
import { MAP_HACK_TECHIES_GAME } from "../Modules/Techies";
import { MAP_HACK_VALIDATE } from "../Service/Validate";

EventsSDK.on("GameEvent", (name, obj) => {
	if (!MAP_HACK_VALIDATE.IsInGame)
		return
	JUNGLE_MAP_HACK_GAME(name, obj)
	MAP_HACK_TECHIES_GAME(name, obj)
	MAP_HACK_WARD_TRACKER_GAME_EVENT(name, obj)
	MAP_HACK_ABILITIES_ENTITIES_GAME_DESTROY(name, obj)
})
