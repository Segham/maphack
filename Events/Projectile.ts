import { EventsSDK } from "wrapper/Imports";
import { ABILITIES_LINEAR_PROJECTILE_CREATE, ABILITIES_LINEAR_PROJECTILE_DESTROY } from "../Modules/Projectile";
import { MAP_HACK_VALIDATE } from "../Service/Validate";

EventsSDK.on("LinearProjectileCreated", proj => {
	if (!MAP_HACK_VALIDATE.IsInGame)
		return
	ABILITIES_LINEAR_PROJECTILE_CREATE(proj)
})

EventsSDK.on("LinearProjectileDestroyed", proj => {
	if (!MAP_HACK_VALIDATE.IsInGame)
		return
	ABILITIES_LINEAR_PROJECTILE_DESTROY(proj)
})
