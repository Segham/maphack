import { EventsSDK } from "wrapper/Imports";
import { ABILITIES_TICK } from "../Modules/Abilities";
import { MAP_HACK_CAMP_TICK } from "../Modules/Camp";
import { ITEMS_TICK } from "../Modules/Items";
import { JUNGLE_MAP_HACK_TICK } from "../Modules/Jungle";
import { CREEP_WAVE_TICK } from "../Modules/Prediction/Creeps/index";
import { MAP_HACK_WARD_TRACKER_TICK } from "../Modules/Prediction/Wards/index";
import { ABILITIES_LINEAR_PROJECTILE_TICK } from "../Modules/Projectile";
import { MAP_HACK_TECHIES_TICK } from "../Modules/Techies";
import { MAP_HACK_VALIDATE } from "../Service/Validate";

EventsSDK.on("Tick", () => {
	if (!MAP_HACK_VALIDATE.IsInGame)
		return
	ITEMS_TICK()
	ABILITIES_TICK()
	CREEP_WAVE_TICK()
	MAP_HACK_CAMP_TICK()
	JUNGLE_MAP_HACK_TICK()
	MAP_HACK_TECHIES_TICK()
	MAP_HACK_WARD_TRACKER_TICK()
	ABILITIES_LINEAR_PROJECTILE_TICK()
})
