import { EventsX } from "X-Core/Imports";
import { MAP_HACK_DATA } from "../data"
import { TemplarAssasinModel } from "../Models/Templar";
import { WispModel } from "../Models/Wisp";
EventsX.on("GameEnded", () => {
	WispModel.Dispose()
	MAP_HACK_DATA.Dispose()
	TemplarAssasinModel.Dispose()
})
